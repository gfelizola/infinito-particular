<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::group(array("before" => "auth"), function()
{
	Route::get('/', 					["as" => "home", 		"uses" => "HomeController@index"]);

	Route::resource('usuarios', 	'UsuarioController');
	Route::resource('mensagens', 	'MensagensController');
	// Route::resource('fotos', 	'FotosController');
	Route::resource('videos', 		'VideosController');
	Route::resource('metas', 		'MetasController');
	Route::resource('performance',  'PerformanceController');
	Route::resource('eventos', 		'EventosController');

	Route::get('usuarios/{id}/professor/{opcao}', 				[ 'as' => 'usuarios.professor', 'uses' => 'UsuarioController@professor']);

	Route::get('fotos', 										[ 'as' => 'fotos.index', 'uses' => 'FotosController@index']);
	Route::get('fotos/album/{id?}', 							[ 'as' => 'fotos.album', 'uses' => 'FotosController@album']);
	Route::post('salva-foto/{tipo}', 							[ 'as' => 'usuario.fotos', 'uses' => 'UsuarioController@salvaFoto']);

	Route::get( 'conhecimento/aluno/{id}', 						[ 'as' => 'performance.aluno', 'uses' => 'PerformanceController@aluno']);
	Route::post('conhecimento/aluno/{id}', 						[ 'as' => 'performance.alunoUpdate', 'uses' => 'PerformanceController@alunoUpdate']);
	Route::get( 'conhecimento/chakra/{aid}/{cid}', 				[ 'as' => 'performance.chakra', 'uses' => 'PerformanceController@show']);
	Route::get( 'conhecimento/chakra/{aid}/{cid}/edit', 		[ 'as' => 'performance.chakra.edit', 'uses' => 'PerformanceController@edit']);
	Route::put( 'conhecimento/chakra/{aid}/{cid}', 				[ 'as' => 'performance.chakra.update', 'uses' => 'PerformanceController@update']);
	Route::post('conhecimento/chakra/{cid}/tecnica', 			[ 'as' => 'performance.chakra.tecnicac', 'uses' => 'PerformanceController@createTecnica']);
	Route::put( 'conhecimento/chakra/{cid}/tecnica/{tid}', 		[ 'as' => 'performance.chakra.tecnicau', 'uses' => 'PerformanceController@updateTecnica']);
	Route::delete('conhecimento/chakra/{cid}/tecnica/{tid}', 	[ 'as' => 'performance.chakra.tecnicad', 'uses' => 'PerformanceController@deleteTecnica']);
	Route::post('conhecimento/chakra/aluno/tecnicas/', 			[ 'as' => 'performance.chakra.tecAluno', 'uses' => 'PerformanceController@tecAluno']);

	Route::get('eventos/calendar/{aid?}', 						[ 'as' => 'eventos.calendar', 'uses' => 'EventosController@calendar']);
	Route::get('eventos/create/{aid?}', 						[ 'as' => 'eventos.createc', 'uses' => 'EventosController@create']);
	Route::get('eventos/edit/{aid}/{id}', 						[ 'as' => 'eventos.editc', 'uses' => 'EventosController@edit']);

	Route::get( 'performance/aluno/{aid?}', 					[ 'as' => 'metas.aluno', 'uses' => 'MetasController@aluno']);
	Route::get( 'performance/create/{tipo}/{aid?}', 			[ 'as' => 'metas.criar', 'uses' => 'MetasController@create']);
	Route::post('performance/{id}/add-foto', 					[ 'as' => 'metas.add-foto', 'uses' => 'MetasController@addFoto']);
	Route::post('performance/{id}/add-nota', 					[ 'as' => 'metas.add-nota', 'uses' => 'MetasController@addNota']);
});

/* ROTAS DE LOGIN, SENHA E CADASTRO */
Route::get('/login', 					["as" => "login", 			"uses" => "LoginController@index"]);
Route::post('/login', 					["as" => "login.validate", 	"uses" => "LoginController@validate"]);
Route::get('/esqueci-a-senha', 			["as" => "login.esqueci", 	"uses" => "LoginController@esqueci"]);
Route::post('/esqueci-a-senha', 		["as" => "login.senha", 	"uses" => "LoginController@enviaSenha"]);
Route::get('/senha/{id}/{token}', 		["as" => "login.reset", 	"uses" => "LoginController@formSenha"]);
Route::post('/senha', 					["as" => "login.atualiza", 	"uses" => "LoginController@validaSenha"]);
Route::get('/logout', 					["as" => "logout", 			"uses" => "LoginController@logout"]);
Route::get('/cadastro', 				["as" => "cadastro", 		"uses" => "LoginController@cadastro"]);
Route::post('/cadastro', 				["as" => "cadastro.save", 	"uses" => "LoginController@cadastroSave"]);
