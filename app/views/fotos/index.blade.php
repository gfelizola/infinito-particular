@extends('master')

@section('content')
	<div class="fotos row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			@include('usuarios.imagens')

			<h1 class="tit-area">Fotos</h1>
			<p>Modo de visualização:
				Álbuns / <a href="{{ route('fotos.album') }}">Fotos</a>
			</p>

			<div class="conteudo-container row">
				<div class="col-xs-12">
					<table class="tabela-fotos albuns">
						<tr>
							@for ($j = 0; $j < count($albuns); $j++)
							<td>
								<?php $a = $albuns[$j]; ?>
								<div class="capa-album">
									<a href="{{ route('fotos.album', [$a->id]) }}">
									@for ($i = 0; $i < ( count($a->fotos) > 3 ? 3 : count($a->fotos) ); $i++)
									<img src="{{ asset($a->fotos[$i]->path) }}" class="img-album img-{{ $i + 1 }}">
									@endfor
									</a>
								</div>
								<p><a href="{{ route('fotos.album', [$a->id]) }}">{{ $a->nome }}</a></p>
							</td>
							@if( ($j + 1) % 4 == 0 )
								</tr><tr>
							@endif
							@endfor
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
@stop