@extends('master')

@section('content')
	<div class="fotos row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			@include('usuarios.imagens')

			<h1 class="tit-area">Fotos</h1>
			<p>Modo de visualização:
				<a href="{{ route('fotos.index') }}">Álbuns</a> / Fotos
			</p>

			<div class="conteudo-container row">
				<div class="col-xs-12">
					<table class="tabela-fotos fotos">
						<?php $c = 1; ?>
						<tr>
							@foreach ($albuns as $a)
								@if ( count($a->fotos) )
									@foreach($a->fotos as $f)
									<td>
										<a rel="fotos-galeria" href="{{ $f->path }}" class="fancybox">
											<img src="{{ Image::url($f->path,200,120,['crop']) }}"><br>
											{{ $f->nome }}
										</a>
									</td>
									@if ( $c++ % 3 == 0 )
										</tr><tr>
									@endif
									@endforeach
								@endif
							@endforeach
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
@stop