@extends('master')

@section('content')
	<div class="eventos row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			<h1 class="tit-area">Eventos</h1>

			@if ( Session::has("message") )
				<div class="alert alert-info" role="alert">{{ Session::pull("message") }}</div>
			@endif

			<div class="conteudo-container row">
				<div class="col-xs-12">
					<div class="row">
						<div class="calendar-header col-xs-12">
							<h3 class="pull-left"></h3>
							<div class="btn-group pull-right">
								<a href="#prev" class="btn btn-link" data-calendar-nav="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
								<a href="#next" class="btn btn-link" data-calendar-nav="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
							</div>
						</div>
					</div>
					<div id="calendario"></div>
				</div>
				
				@if( $usuario->isProfessor() )
				<div class="col-xs-12">
					<a href="{{ route('eventos.createc', [$aid]) }}" class="btn btn-primary">Adicionar evento</a>
				</div>
				@endif
			</div>
		</div>
	</div>

	<div class="modal fade" tabindex="-1" role="dialog" id="events-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
					<h3 class="modal-title">Modal title</h3>
				</div>
				<div class="modal-body">
					<p>One fine body&hellip;</p>
				</div>
				<div class="modal-footer">
					@if( $usuario->isProfessor() )
						<a href="{{ route('eventos.editc', [$aid, '0']) }}" class="btn btn-editar-evento btn-default pull-left">Editar evento</a>
						<button type="button" class="btn btn-excluir-evento btn-danger pull-left">Apagar evento</button>
					@endif
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>

	<script>
		var eventos = {{ $eventos }};

	</script>
@stop