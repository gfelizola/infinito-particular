@extends('master')

@section('content')
	<div class="eventos row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			<h1 class="tit-area">Eventos</h1>

			@if ( $errors->has() )
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p><i class="fa fa-ban"></i> <b>Aviso!</b></p>
				@foreach( $errors->all('<p>:message</p>') as $message )
					{{ $message }}
				@endforeach
			</div>
			@endif

			<div class="conteudo-container row">
				@if ( isset($evento) )
					{{ Form::model($evento, ['route' => ['eventos.update', $evento->id], 'method' => 'put']) }}
				@else
					{{ Form::open(['route' => 'eventos.store', 'method' => 'POST']) }}
				@endif

				{{ Form::hidden( 'aluno', $aid ) }}
				
				<div class="col-xs-12 form-group {{{ $errors->has('nome') ? 'has-error' : '' }}}">
					{{ Form::label("nome", "Nome:") }}
					{{ Form::text("nome", Input::old('nome'), array( "class" => "form-control" )) }}
				</div>
				
				<div class="col-xs-12 form-group {{{ $errors->has('descricao') ? 'has-error' : '' }}}">
					{{ Form::label("descricao", "Descrição:") }}
					{{ Form::textarea("descricao", Input::old('descricao'), array( "class" => "form-control" )) }}
				</div>
				
				<div class="col-xs-12 form-group {{{ $errors->has('data_evento') ? 'has-error' : '' }}}">
					{{ Form::label("data_evento", "Data:") }}
					{{ Form::text("data_evento", Input::old('data_evento'), array( "class" => "form-control" )) }}
				</div>
				
				<div class="col-xs-12 form-group {{{ $errors->has('tipo') ? 'has-error' : '' }}}">
					<label>Tipo de evento:</label>

					<div class="row">
						<div class="col-xs-6 col-sm-3">
							<label for="tipo_aula">
								<input type="radio" name="tipo" id="tipo_aula" value="1" checked />
								Aula Marcada
							</label>
						</div>
						<div class="col-xs-6 col-sm-3">
							<label for="tipo_fora">
								<input type="radio" name="tipo" id="tipo_fora" value="2" />
								Evento Fora
							</label>
						</div>
						<div class="col-xs-6 col-sm-3">
							<label for="tipo_especial">
								<input type="radio" name="tipo" id="tipo_especial" value="3" />
								Evento Especial
							</label>
						</div>
						<div class="col-xs-6 col-sm-3">
							<label for="tipo_outros">
								<input type="radio" name="tipo" id="tipo_outros" value="4" />
								Outros Eventos
							</label>
						</div>
					</div>
				</div>
				
				<div class="col-xs-12">
					<button class="btn btn-primary btn-flat">SALVAR</button>
				</div>
			</div>
		</div>
	</div>
@stop