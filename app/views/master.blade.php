<!doctype html>
<html class="no-js" lang="">
    @include('head')
    <body class="{{ $mainClass }}">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <header id="main-header" class="container">
            <div class="col-xs-12 text-center">
                <h1 class="logo"><a href="{{ route('home') }}">Infinito Particular</a></h1>
            </div>
        </header>

        @if ( ! is_null($usuario) )
            <nav id="main-menu">
                <div class="container">
                    <a href="#menu" class="btn-open-menu hidden-md hidden-lg"><i class="glyphicon glyphicon-menu-hamburger"></i></a>
                    <ul>
                        @foreach ($menu as $m)
                            <li><a href="{{ $m['url'] }}" class="{{ $m['ativo'] ? 'active' : '' }}">{{ $m['nome'] }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </nav>
        @endif

        <!-- Add your site or application content here -->
        <section id="main-container" class="container">
            @section('content')
                Conteudo
            @show
        </section>


        <footer id="main-footer">
            <p>© Infinito particular {{ date('Y') }}. Todos os direitos reservados</p>
        </footer>

        @include('scripts')
    </body>
</html>
