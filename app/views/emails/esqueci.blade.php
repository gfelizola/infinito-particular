<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Infinito</h2>

		<div>
			<p>Você está recebendo este e-mail porque solicitou a troca de senha no site Infinito.</p>
			<p>Para renovar seu acesso <a href="{{ URL::route('login.reset', array($id, $codigo)) }}">clique aqui</a>.</p>
			<p>Caso não tenha solicitado, peço que ignore este e-mail.</p>
		</div>
	</body>
</html>
