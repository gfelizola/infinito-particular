@extends('master')

@section('content')
	<div class="row">
		<div class="col-xs-12 col-md-8 col-md-offset-2">

			@include('usuarios.imagens')

			<h1>Olá
			@if ( $usuario->isProfessor() )
				@if ( $usuario->sexo == "F" )
					Professora
				@else
					Professor
				@endif
			@endif

			{{ $perfil->nome_completo }},</h1>
		</div>
		<div class="col-xs-12 col-md-10 col-md-offset-2">
			<div class="row">
				<div class="conteudo-container col-xs-12 col-md-10">
					@if ( count( $historicos ) )
					<ul class="list-unstyled historico">
							{{-- expr --}}
						@foreach ($historicos as $h)
							<li>
								<i class="glyphicon glyphicon-{{ $h->icone }}"></i>
								{{ $h->mensagem }}
							</li>
						@endforeach
					</ul>
					@endif
				</div>
				@include('minicalendar')
			</div>
		</div>
	</div>
@stop