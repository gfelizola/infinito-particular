@extends('master')

@section('content')
	<div class="metas row">
		<div class="col-xs-12 col-md-8 col-md-offset-2">
			<h1 class="tit-area">Performance</h1>

			@if ( $errors->has() )
			<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p><b>Aviso!</b></p>
				@foreach( $errors->all('<p>:message</p>') as $message )
					{{ $message }}
				@endforeach
			</div>
			@endif

			<div class="conteudo-container row">
				@if ( isset($meta) )
				{{ Form::model($meta, array('route' => ['metas.update', $meta->id], 'method' => 'put', 'files' => true)) }}
				{{ Form::hidden('aid', $meta->aluno->id ) }}
				{{ Form::hidden('tipo', $meta->tipo ) }}
				@else
				{{ Form::open(array('route' => array('metas.store'), 'method' => 'post', 'files' => true)) }}
				{{ Form::hidden('aid', Route::input('aid') ) }}
				{{ Form::hidden('tipo', Route::input('tipo') ) }}
				@endif

				<div class="col-xs-12 form-group {{{ $errors->has('nome') ? 'has-error' : '' }}}">
					{{ Form::label("nome", "Nome") }}
					{{ Form::text("nome", Input::old('nome'), array( "class" => "form-control" )) }}
				</div>
				<div class="col-xs-12 form-group {{{ $errors->has('meta') ? 'has-error' : '' }}}">
					{{ Form::label("meta", "Meta") }}
					{{ Form::text("meta", Input::old('meta'), array( "class" => "form-control" )) }}
					<p class="help-block">(Ex. 12 minutos | 0-4-5-3)</p>
				</div>
				<div class="col-xs-12 form-group {{{ $errors->has('video') ? 'has-error' : '' }}}">
					{{ Form::label("video", "Link para vídeo") }}
					{{ Form::text("video", Input::old('video'), array( "class" => "form-control" )) }}
				</div>
				<div class="col-xs-12 form-group {{{ $errors->has('foto') ? 'has-error' : '' }}}">
					{{ Form::label("foto", "Imagem") }}
					{{ Form::file("foto", Input::old('foto'), array( "class" => "form-control" )) }}
				</div>
                    <div class="col-xs-12 form-group">
                        <button class="btn btn-primary btn-flat">SALVAR</button>
                    </div>

				{{ Form::close() }}
			</div>
		</div>
	</div>
@stop