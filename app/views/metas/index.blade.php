@extends('master')

@section('content')
	<div class="metas row">
		<div class="col-xs-12 col-md-8 col-md-offset-2">
			@include('usuarios.imagens')

			<h1 class="tit-area">Performance</h1>

			@if ( Session::has("message") )
				<div class="alert alert-info" role="alert">{{ Session::pull("message") }}</div>
			@endif

			<p>Ásanas - Posições <a href="http://www.vocenoinfinito.com/" target="_blank">(Clique para mais info)</a></p>
			<div class="conteudo-container asanas metas-container flexslider">
				<ul class="slider">
					@foreach ($posicoes as $meta)
					<li class="{{ ( count( $meta->notas ) && $meta->notas->sortByDesc('created_at')->first()->valor == 100 ) ? "concluida" : "" }}">
						<div>
							<a href="{{ route('metas.show', [$meta->id]) }}" data-fancybox-type="iframe" class="link-meta">
								@if ( ! empty($meta->foto) )
								
								<img src="/uploads/metas/{{ $meta->foto }}" alt="">
								@else
								<p class="nome">{{ $meta->nome }}</p>
								@endif
							</a>
							<div class="inferior">
								<div class="posicao">
									@if ( count( $meta->notas ) )
										<div class="percentual" style="width:{{ $meta->notas->sortByDesc('created_at')->first()->valor }}%"></div>
									@else
										<div class="percentual" style="width:0"></div>
									@endif
								</div>
								<p class="meta">{{ $meta->meta }}</p>
							</div>
						</div>
						@if ( $usuario->isProfessor() && $aluno->professor->id == $usuario->id )
							<a href="{{ route('metas.edit', [ $meta->id ]) }}" class="btn btn-link"><i class="glyphicon glyphicon-pencil"></i> Editar</a>
						@endif
					</li>
					@endforeach
					@if ( $usuario->isProfessor() && $aluno->professor->id == $usuario->id )
					<li>
						<div class="adicionar">
							<a href="{{ route('metas.criar', [ 1, Route::input('aid') ]) }}" class="btn btn-primary btn-block">Adicionar meta</a>
						</div>
					</li>
					@endif
				</ul>
			</div>

			<p>Respiração <a href="http://www.vocenoinfinito.com/" target="_blank">(Clique para mais info)</a></p>
			<div class="conteudo-container respiracao metas-container flexslider">
				<ul class="slider">
					@foreach ($respiracoes as $meta)
					<li class="{{ ( count( $meta->notas ) && $meta->notas->sortByDesc('created_at')->first()->valor == 100 ) ? "concluida" : "" }}">
						<div>
							<a href="{{ route('metas.show', [$meta->id]) }}" data-fancybox-type="iframe" class="link-meta">
								<p class="nome">{{ $meta->nome }}</p>
							</a>
							<div class="inferior">
								<div class="posicao">
									@if ( count( $meta->notas ) )
										<div class="percentual" style="width:{{ $meta->notas->sortByDesc('created_at')->first()->valor }}%"></div>
									@else
										<div class="percentual" style="width:0"></div>
									@endif
								</div>
								<p class="meta">{{ $meta->meta }}</p>
							</div>
						</div>
						@if ( $usuario->isProfessor() && $aluno->professor->id == $usuario->id )
							<a href="{{ route('metas.edit', [ $meta->id ]) }}" class="btn btn-link"><i class="glyphicon glyphicon-pencil"></i> Editar</a>
						@endif
					</li>
					@endforeach
					@if ( $usuario->isProfessor() && $aluno->professor->id == $usuario->id )
					<li>
						<div class="adicionar">
							<a href="{{ route('metas.criar', [ 2, Route::input('aid') ]) }}" class="btn btn-primary btn-block">Adicionar meta</a>
						</div>
					</li>
					@endif
				</ul>
			</div>

			<p>Meditação <a href="http://www.vocenoinfinito.com/" target="_blank">(Clique para mais info)</a></p>
			<div class="conteudo-container meditacao metas-container flexslider">
				<ul class="slider">
					@foreach ($meditacoes as $meta)
					<li class="{{ ( count( $meta->notas ) && $meta->notas->sortByDesc('created_at')->first()->valor == 100 ) ? "concluida" : "" }}">
						<div>
							<a href="{{ route('metas.show', [$meta->id]) }}" data-fancybox-type="iframe" class="link-meta">
								<p class="nome">{{ $meta->nome }}</p>
							</a>
							<div class="inferior">
								<div class="posicao">
									@if ( count( $meta->notas ) )
										<div class="percentual" style="width:{{ $meta->notas->sortByDesc('created_at')->first()->valor }}%"></div>
									@else
										<div class="percentual" style="width:0"></div>
									@endif
								</div>
								<p class="meta">{{ $meta->meta }}</p>
							</div>
						</div>
						@if ( $usuario->isProfessor() && $aluno->professor->id == $usuario->id )
							<a href="{{ route('metas.edit', [ $meta->id ]) }}" class="btn btn-link"><i class="glyphicon glyphicon-pencil"></i> Editar</a>
						@endif
					</li>
					@endforeach
					@if ( $usuario->isProfessor() && $aluno->professor->id == $usuario->id )
					<li>
						<div class="adicionar">
							<a href="{{ route('metas.criar', [ 3, Route::input('aid') ]) }}" class="btn btn-primary btn-block">Adicionar meta</a>
						</div>
					</li>
					@endif
				</ul>
			</div>

			<p>Limpeza <a href="http://www.vocenoinfinito.com/" target="_blank">(Clique para mais info)</a></p>
			<div class="conteudo-container limpeza metas-container flexslider">
				<ul class="slider">
					@foreach ($limpezas as $meta)
					<li class="{{ ( count( $meta->notas ) && $meta->notas->sortByDesc('created_at')->first()->valor == 100 ) ? "concluida" : "" }}">
						<div>
							<a href="{{ route('metas.show', [$meta->id]) }}" data-fancybox-type="iframe" class="link-meta">
								<p class="nome">{{ $meta->nome }}</p>
							</a>
							<div class="inferior">
								<div class="posicao">
									@if ( count( $meta->notas ) )
										<div class="percentual" style="width:{{ $meta->notas->sortByDesc('created_at')->first()->valor }}%"></div>
									@else
										<div class="percentual" style="width:0"></div>
									@endif
								</div>
								<p class="meta">{{ $meta->meta }}</p>
							</div>
						</div>
						@if ( $usuario->isProfessor() && $aluno->professor->id == $usuario->id )
							<a href="{{ route('metas.edit', [ $meta->id ]) }}" class="btn btn-link"><i class="glyphicon glyphicon-pencil"></i> Editar</a>
						@endif
					</li>
					@endforeach
					@if ( $usuario->isProfessor() && $aluno->professor->id == $usuario->id )
					<li>
						<div class="adicionar">
							<a href="{{ route('metas.criar', [ 4, Route::input('aid') ]) }}" class="btn btn-primary btn-block">Adicionar meta</a>
						</div>
					</li>
					@endif
				</ul>
			</div>
		</div>
	</div>
@stop