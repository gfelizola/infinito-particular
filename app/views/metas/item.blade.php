@extends('master-lightbox')

@section('content')
	<div class="container-fluid metas">
		<div class="row meta-item metas-container">
			@if ( $meta->tipo == Meta::TIPO_ASANA && ! empty( $meta->foto ) )
			<div class="col-xs-12 col-sm-4 text-center">
				<img src="/uploads/metas/{{ $meta->foto }}" alt="{{ $meta->nome }}">
			</div>
			<div class="col-xs-12 col-sm-6">
			@else
			<div class="col-xs-12 col-sm-6 col-sm-offset-3 meta-nome">
				<h3 class="text-center">{{ $meta->nome }}</h3>
			@endif
				@if ( count( $notas ) && $notas->first()->valor == 100 )
				<div class="concluida">
				@else
				<div>
				@endif

					<div class="posicao">
						@if ( count( $notas ) )
							<div class="percentual" style="width:{{ $notas->first()->valor }}%"></div>
						@else
							<div class="percentual" style="width:0"></div>
						@endif
					</div>
					<p class="meta">{{ $meta->meta }}</p>
				</div>
			</div>

			<div class="col-xs-12">
				@if ( ! empty( $meta->video ) )
					<p class="link-video">
						<a href="{{ $meta->video }}" target="_blank">Clique aqui para ver vídeo exemplo</a>
					</p>
				@endif
			</div>

			@if ( Session::has("message_foto") )
				<div class="col-xs-12">
					<div class="alert alert-info" role="alert">{{ Session::pull("message_foto") }}</div>
				</div>
			@endif

			{{-- LISTAGEM DE FOTOS --}}

			<div class="col-xs-12">

				{{-- @if ( count( $meta->fotos ) ) --}}
					<h3>Fotos:</h3>

					<div class="fotos">
						<ul class="slider">
							@foreach($meta->fotos as $foto)
								<li><a href="/uploads/metas/meta_{{ $meta->id }}/{{ $foto->url }}" class="link-foto">
									<img src="{{ Image::url('/uploads/metas/meta_' . $meta->id .'/'. $foto->url ,140,90,['crop']) }}" alt="{{ $foto->id }}">
								</a></li>
							@endforeach
						</ul>
					</div>
				{{-- @endif --}}
			</div>

			{{-- FORMULÁRIO PARA ADICIONAR FOTOS --}}
			@if ( $errors->has('imagem') )
			<div class="col-xs-12">
				<div class="alert alert-danger alert-dismissable">
					<p><i class="fa fa-ban"></i> <b>Aviso!</b></p>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					@foreach( $errors->all('<p>:message</p>') as $message )
						{{ $message }}
					@endforeach
				</div>
			</div>
			@endif

			@if ( $usuario->isProfessor() && $meta->aluno->professor->id == $usuario->id )
			<div class="col-xs-12">
				<div class="panel panel-primary">
					<div class="panel-body">
						<p>Selecione uma foto para adicionar a meta</p>
						{{ Form::open(['route' => ['metas.add-foto'], 'method' => 'post', 'files' => true]) }}
						{{ Form::hidden('meta', $meta->id ) }}
						
						<div class="input-group">
							{{ Form::file('imagem', ['class' => 'form-control']) }}
							<span class="input-group-btn">
								{{ Form::submit('Adicionar Foto', ['class' => 'btn btn-warning']); }}
							</span>
						</div>
						{{ Form::close() }}
					</div>
				</div>
			</div>
			@endif

			{{-- LISTAGEM DE NOTAS --}}

			<div class="col-xs-12 notas">
				<h3>Notas:</h3>

				<ul>
					@forelse($notas as $nota)
						<li>{{ $nota->created_at->format('d/m') }} - {{ $nota->descricao }}</li>
					@empty
						<li>Nenhuma nota cadastrada até o momento</li>
					@endforelse
				</ul>
				@if ( $usuario->isProfessor() && $meta->aluno->professor->id == $usuario->id )
				<a href="#add-nota" class="btn btn-primary btn-add-nota">Adicionar nota</a>
				@endif
			</div>

			{{-- FORMULÁRIO PARA ADICIONAR NOTAS --}}

			@if ( $usuario->isProfessor() && $meta->aluno->professor->id == $usuario->id )
				@if ( Session::has("message_nota") )
					<div class="col-xs-12">
						<div class="alert alert-info" role="alert">{{ Session::pull("message_nota") }}</div>
					</div>
				@endif

				@if ( $errors->has('descricao') || $errors->has('valor') )
				<div class="col-xs-12">
					<div class="alert alert-danger alert-dismissable">
						<p><i class="fa fa-ban"></i> <b>Aviso!</b></p>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						@foreach( $errors->all('<p>:message</p>') as $message )
							{{ $message }}
						@endforeach
					</div>
				</div>
				<div class="col-xs-12 form-nota open">
				@else
				<div class="col-xs-12 form-nota">
				@endif

					<div class="panel panel-primary">
						<div class="panel-body">
							{{ Form::open(['route' => ['metas.add-nota'], 'method' => 'post']) }}
							{{ Form::hidden('meta', $meta->id ) }}
							
							<div class="form-group {{{ $errors->has('descricao') ? 'has-error' : '' }}}">
								{{ Form::label("valor", "Nota (de 0 a 100)") }}
								{{ Form::number("valor", Input::old('valor'), array( "class" => "form-control" )) }}
								{{-- <input id="valor" name="valor" type="range" min="0" max="100" step="1" class="form-control" /> --}}
							</div>
							<div class="form-group {{{ $errors->has('descricao') ? 'has-error' : '' }}}">
								{{ Form::label("descricao", "Descricao") }}
								{{ Form::text("descricao", Input::old('video'), array( "class" => "form-control" )) }}
							</div>
							
							{{ Form::submit('Enviar Nota', ['class' => 'btn btn-warning']); }}
							{{ Form::close() }}
						</div>
					</div>
				</div>
			@endif
		</div>
	</div>
@stop