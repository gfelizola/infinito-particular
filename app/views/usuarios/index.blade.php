@extends('master')

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-10 col-sm-offset-2">
			<h1>Quem faz parte do Infinito</h1>
		</div>

		<div class="col-xs-12 col-sm-10 col-sm-offset-2">
			<div class="row">
				<div class="col-xs-12 col-sm-10">
					<div class="conteudo-container row">
						<div class="col-xs-12 col-sm-9">
							{{ Form::open(array('route' => 'usuarios.index', 'method' => 'GET')) }}
							{{ Form::hidden('p',Input::get('p')) }}
							{{ Form::label('q','Buscar') }}<br>
							{{ Form::text('q',Input::get('q')) }}
							<button class="btn btn-link"><i class="glyphicon glyphicon-search"></i></button>
							{{ Form::close() }}
						</div>
						<div class="col-xs-12 col-sm-3 text-right">
							@if ( empty( Input::get("p") ) && $usuario->isProfessor() )
								<a href="/usuarios?p={{ $usuario->id }}" class="btn btn-primary">Mostrar meus alunos</a>
							@else
								<a href="/usuarios" class="btn btn-primary">Mostrar todos</a>
							@endif
						</div>
						<div class="col-xs-12">
							<ul class="lista-usuarios list-unstyled row">
								@foreach ($usuarios as $u)
									<li class="col-xs-6 col-sm-3 text-center">
										<a href="{{ route('usuarios.show', [$u->id]) }}" class="thumbnail">
											<div class="perfil"  style="background-image: url('{{ $u->imagem_perfil ? "/uploads/" . $u->imagem_perfil : "/img/no-avatar.gif" }}')">
												{{-- @if( empty( $u->imagem_perfil ) ) --}}
												{{-- <img src="/img/no-avatar.gif" alt="{{ $u->nome_completo }}"> --}}
												{{-- @else --}}
												{{-- <img src="/uploads/{{ $u->imagem_perfil }}" alt="{{ $u->nome_completo }}"> --}}
												{{-- @endif --}}
											</div>
											<p>{{ $u->nome_completo }}</p>
										</a>
									</li>
								@endforeach
							</ul>
						</div>
						<div class="col-xs-12 text-center">
							{{ $usuarios->links() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop