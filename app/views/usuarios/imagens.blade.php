<div class="row">
	<div class="imagens-perfil">
		<div class="capa" style="background-image: url('/uploads/{{ $perfil->imagem_capa }}')">
			@if( ! empty( $perfil->imagem_capa ) )
				{{-- <img src="/uploads/{{ $perfil->imagem_capa }}" alt="{{ $perfil->nome_completo }}"> --}}
				{{-- <img src="{{ Image::url('/uploads/' . $perfil->imagem_capa, 780, 200,['crop']) }}" alt="{{ $perfil->nome_completo }}"> --}}
			@endif
			@if ( $usuario->id == $perfil->id )
				<a href="#formFotoCapa" class="btn-trocar-imagem btn-imagem-capa fancybox"><i class="glyphicon glyphicon-pencil"></i></a>
			@endif
		</div>
		<div class="perfil" style="background-image: url('{{ $perfil->imagem_perfil ? "/uploads/" . $perfil->imagem_perfil : "/img/no-avatar.gif" }}')">
			@if( empty( $perfil->imagem_perfil ) )
				{{-- <img src="/img/no-avatar.gif" alt="{{ $perfil->first_name . " " . $perfil->last_name }}"> --}}
			@else
				{{-- <img src="{{ '/uploads/' . $perfil->imagem_perfil }}" alt="{{ $perfil->nome_completo }}"> --}}
				{{-- <img src="{{ Image::url('/uploads/' . $perfil->imagem_perfil ,150,150,['crop']) }}" alt="{{ $perfil->nome_completo }}"> --}}

			@endif
			@if ( $usuario->id == $perfil->id )
				<a href="#formFotoPerfil" class="btn-trocar-imagem btn-imagem-perfil fancybox"><i class="glyphicon glyphicon-pencil"></i></a>
			@endif
		</div>
	</div>
</div>

@if ( $usuario->id == $perfil->id )
	<div id="formFotoPerfil" class="esconde">
		{{ Form::open(array('url' => '/salva-foto/perfil', 'files' => true)) }}
		{{ Form::hidden('retorno', Route::getCurrentRoute()->getUri() ); }}
		<h2>Escolha uma foto para seu perfil:</h2>

		@if ( Input::get('image-error-perfil','false') == 'true' )
			<div class="form-group has-error">
				{{ Form::file('imagem', ['class' => 'form-control']) }}
				<label for="imagem" class="control-label">Formato de imagem inválido</label>
			</div>
		@else
			<div class="form-group">
				{{ Form::file('imagem', ['class' => 'form-control']) }}
			</div>
		@endif

		<div class="form-group">
			{{ Form::submit('Enviar', ['class' => 'btn']); }}
		</div>
		{{ Form::close() }}
	</div>

	<div id="formFotoCapa" class="esconde">
		{{ Form::open(array('url' => '/salva-foto/capa', 'files' => true)) }}
		{{ Form::hidden('retorno', Route::getCurrentRoute()->getUri() ); }}
		<h2>Escolha uma foto para sua capa:</h2>

		@if ( Input::get('image-error-capa','false') == 'true' )
			<div class="form-group has-error">
				{{ Form::file('imagem', ['class' => 'form-control']) }}
				<label for="imagem" class="control-label">Formato de imagem inválido</label>
			</div>
		@else
			<div class="form-group">
				{{ Form::file('imagem', ['class' => 'form-control']) }}
			</div>
		@endif

		<div class="form-group">
			{{ Form::submit('Enviar', ['class' => 'btn']); }}
		</div>
		{{ Form::close() }}
	</div>
@endif