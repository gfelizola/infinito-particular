@extends('master')

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">

			@include('usuarios.imagens')

			<h1>Meu perfil</h1>

			<p><a href="{{ route('usuarios.show', [$perfil->id]) }}">Dados</a> / Editar</p>

			@if ( $errors->has() )
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p><b>Aviso!</b></p>
                @foreach( $errors->all('<p>:message</p>') as $message )
                    {{ $message }}
                @endforeach
            </div>
            @endif

			<div class="conteudo-container row">
				{{ Form::model($perfil, array('route' => array('usuarios.update', $perfil->id), 'method' => 'put')) }}

					<div class="col-xs-12 col-sm-6 form-group {{{ $errors->has('first_name') ? 'has-error' : '' }}}">
                        {{ Form::label("first_name", "Nome") }}
                        {{ Form::text("first_name", null, array( "class" => "form-control" )) }}
                    </div>
                    <div class="col-xs-12 col-sm-6 form-group {{{ $errors->has('last_name') ? 'has-error' : '' }}}">
                        {{ Form::label("last_name", "Sobrenome") }}
                        {{ Form::text("last_name", null, array( "class" => "form-control" )) }}
                    </div>
                    <div class="col-xs-12 col-sm-6 form-group {{{ $errors->has('last_name') ? 'has-error' : '' }}}">
                        {{ Form::label("sexo", "Sexo") }}
                        {{ Form::select('sexo', Config::get('infinito.sexo'), null, array( "class" => "form-control" ) ); }}
                    </div>
                    <div class="col-xs-12 col-sm-6 col-sm-6 form-group {{{ $errors->has('estado_civil') ? 'has-error' : '' }}}">
                        {{ Form::label("estado_civil", "Estado Civil") }}
                        {{ Form::select('estado_civil', Config::get('infinito.estado_civil'), null, array( "class" => "form-control" ) ); }}
                    </div>
                    <div class="col-xs-12 col-sm-6 form-group {{{ $errors->has('peso') ? 'has-error' : '' }}}">
                        {{ Form::label("peso", "Peso") }}
                        {{ Form::text("peso", null, array( "class" => "form-control" )) }}
                    </div>
                    <div class="col-xs-12 col-sm-6 form-group {{{ $errors->has('altura') ? 'has-error' : '' }}}">
                        {{ Form::label("altura", "Altura") }}
                        {{ Form::text("altura", null, array( "class" => "form-control" )) }}
                    </div>
                    <div class="col-xs-12 form-group {{{ $errors->has('data_nascimento') ? 'has-error' : '' }}}">
                        {{ Form::label("data_nascimento", "Data de Nascimento") }}
                        {{ Form::text("data_nascimento", null, array( "class" => "form-control date-field" )) }}
                    </div>
                    <div class="col-xs-12 form-group {{{ $errors->has('profissao') ? 'has-error' : '' }}}">
                        {{ Form::label("profissao", "Profissão") }}
                        {{ Form::select('profissao', $profissoes, null, array( "class" => "form-control" ) ); }}
                    </div>
                    <div class="col-xs-12 form-group {{{ $errors->has('nacionalidade') ? 'has-error' : '' }}}">
                        {{ Form::label("nacionalidade", "Nacionalidade") }}
                        {{ Form::select('nacionalidade', array("", 'B' => 'Brasileira', 'O' => 'Outra'), null, array( "class" => "form-control" ) ); }}
                    </div>
                    <div class="col-xs-12 form-group {{{ $errors->has('telefone') ? 'has-error' : '' }}}">
                        {{ Form::label("telefone", "Telefone") }}
                        {{ Form::text("telefone", null, array( "class" => "form-control phone-field" )) }}
                    </div>
                    <div class="col-xs-12 form-group {{{ $errors->has('endereco') ? 'has-error' : '' }}}">
                        {{ Form::label("endereco", "Endereço") }}
                        {{ Form::text("endereco", null, array( "class" => "form-control" )) }}
                    </div>
                    <div class="col-xs-12 form-group {{{ $errors->has('facebook') ? 'has-error' : '' }}}">
                        {{ Form::label("facebook", "Endereço nas redes sociais") }}
                        {{ Form::textarea("facebook", null, array( "class" => "form-control", "rows" => "3" )) }}
                        <p class="help-text">Colocar um link por linha.</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 form-group {{{ $errors->has('inicio_infinito') ? 'has-error' : '' }}}">
                        {{ Form::label("inicio_infinito", "Data de início da prática no Infinito") }}
                        {{ Form::text("inicio_infinito", null, array( "class" => "form-control date-field" )) }}
                    </div>
                    <div class="col-xs-12 col-sm-6 form-group {{{ $errors->has('inicio_yoga') ? 'has-error' : '' }}}">
                        {{ Form::label("inicio_yoga", "Data de início da prática de Yoga") }}
                        {{ Form::text("inicio_yoga", null, array( "class" => "form-control date-field" )) }}
                    </div>
                    <div class="col-xs-12 form-group {{{ $errors->has('objetivos') ? 'has-error' : '' }}}">
                        {{ Form::label("objetivos", "Objetivos com a prática") }}
                        {{ Form::textarea("objetivos", null, array( "class" => "form-control", "rows" => 5 )) }}
                    </div>
                    <div class="col-xs-12 form-group {{{ $errors->has('observacoes') ? 'has-error' : '' }}}">
                        {{ Form::label("observacoes", "Observações médicas") }}
                        {{ Form::textarea("observacoes", null, array( "class" => "form-control", "rows" => 5 )) }}
                    </div>
                    <div class="col-xs-12 form-group">
                        <button class="btn btn-primary btn-flat">SALVAR ALTERAÇÕES</button>
                    </div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
@stop