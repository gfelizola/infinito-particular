@extends('master')

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">

			@include('usuarios.imagens')

			<h1>{{ $perfil->nome_completo }}</h1>

			@if ( $usuario->id == $perfil->id )
			<p>Dados / <a href="{{ route('usuarios.edit', [$perfil->id]) }}">Editar</a></p>
			@endif

			@if ( Session::has("message") )
				<div class="alert alert-info" role="alert">{{ Session::pull("message") }}</div>
			@endif

			@if ( $errors->has() )
			<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				@foreach( $errors->all('<p>:message</p>') as $message )
					{{ $message }}
				@endforeach
			</div>
			@endif
		</div>

		<div class="col-xs-12 col-sm-10 col-sm-offset-2">
			<div class="row">
				<div class="col-xs-12 col-sm-10">
					<div class="conteudo-container row">
						<div class="col-xs-12 col-sm-9">
							{{-- <p><b>Sexo:</b><br>{{ $perfil->sexo_nome }}</p> --}}
							{{-- <p><b>Estado Civil:</b><br>{{ $perfil->estado_civil_nome }}</p> --}}
							<p><b>Profissão:</b><br>{{ $perfil->profissao }}</p>
							{{-- <p><b>Nacionalidade:</b><br>{{ $perfil->nacionalidade_nome }}</p> --}}
						</div>
					
						<div class="col-xs-12 col-sm-3">
						@if ( $usuario->isAdmin() )
							@if ( $perfil->isProfessor() )
								<a href="{{ route('usuarios.professor',[$perfil->id,0]) }}" class="btn btn-primary btn-block">Tornar não professor</a>
							@else
								<a href="{{ route('usuarios.professor',[$perfil->id,1]) }}" class="btn btn-primary btn-block">Tornar professor</a>
							@endif
						@endif
						@if ( $usuario->isProfessor() && $perfil->professor )
							@if ( $perfil->professor->id == $usuario->id )
								<a href="{{ route('metas.aluno',[$perfil->id]) }}" class="btn btn-primary btn-block">Editar Performance</a>
								<a href="{{ route('performance.aluno', [$perfil->id]) }}" class="btn btn-primary btn-block">Editar Conhecimento</a>
								{{-- <a href="#" class="btn btn-primary btn-block">Editar Calendário</a> --}}
							@endif
						@endif
						@if ( $usuario->isProfessor() )
							<a href="{{ route('eventos.calendar', [$perfil->id]) }}" class="btn btn-primary btn-block">Editar eventos</a>
						@endif
						</div>
					</div>
					
					@if ( $usuario->id != $perfil->id )
					<div class="conteudo-container row">
						<div class="col-xs-12 col-sm-9">
							<p>Envie uma mensagem para {{ $perfil->nome }}:</p>
							{{ Form::open( [ 'route' => 'mensagens.store', 'method' => 'POST' ] ) }}
							{{ Form::hidden('from', $usuario->id) }}
							{{ Form::hidden('to', $perfil->id) }}
							{{ Form::hidden('retorno', Request::url() ); }}
					
							<div class="row">
								<div class="col-xs-12 form-group">
									{{ Form::text('assunto', '', ['placeholder' => 'Assunto:', 'class' => 'form-control'] ) }}
								</div>
								<div class="col-xs-12 form-group">
									{{ Form::textarea('mensagem', '', ['rows' => 5, 'class' => 'form-control'] ) }}
								</div>
								<div class="col-xs-12 form-group">
									<button class="btn btn-primary pull-right">Enviar</button>
								</div>
							</div>
					
							{{ Form::close() }}
						</div>
					</div>
					@endif
				</div>
				@if ( $usuario->id == $perfil->id )
				@include('minicalendar')
				@endif
			</div>
		</div>
	</div>
@stop