@extends('master')

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			@include('usuarios.imagens')

			<h1>Mensagens</h1>

			<div class="conteudo-container row">
				<div class="col-xs-12">
					{{ Form::open(array('route' => 'usuarios.index', 'method' => 'GET')) }}
					{{ Form::label('q','Buscar') }}<br>
					{{ Form::text('q','') }}
					<button class="btn btn-link"><i class="glyphicon glyphicon-search"></i></button>
					{{ Form::close() }}
				</div>
				<div class="col-xs-12">
					<table class="table table-hover table-condensed table-responsive">
						@foreach ( $conversas  as $conversa)
							<tr>
								<td><b>De:</b> {{ $conversa->autor->nome_completo }}</td>
								<td><b>Assunto:</b> <a href="{{ route('mensagens.show', [$conversa->id]) }}">{{ $conversa->assunto }}</a></td>
								<td><b>Data:</b> {{ date( 'd/m/Y H:i', strtotime($conversa->updated_at)) }}</td>
							</tr>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
@stop