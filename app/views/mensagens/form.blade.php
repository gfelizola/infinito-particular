@extends('master')

@section('content')
	<div class="row mensagens">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			@include('usuarios.imagens')

			<div class="row">
				<h1 class="tit-area col-xs-12 col-sm-8">Mensagens</h1>
				<div class="col-xs-12 col-sm-4 text-right">
					<a href="{{ route('mensagens.index') }}"><i class="glyphicon glyphicon-chevron-left"></i> Voltar</a>
				</div>
			</div>

			<div class="conteudo-container row">
				<div class="col-xs-12">
					<div class="cabecalho">
						<div class="row">
							<span class="info info-de col-xs-12 col-sm-4">De: 
							@if($conversa->autor)
								<a href="{{ route('usuarios.show', [$conversa->autor->id]) }}">{{ $conversa->autor->nome_completo }}</a>
							@else
								Não identificado
							@endif
							</span>
							<span class="info info-assunto col-xs-12 col-sm-4">Assunto: {{ $conversa->assunto }}</span>
							<span class="info info-data col-xs-12 col-sm-4 text-right">Data: {{ date( 'd/m/Y H:i', strtotime($conversa->updated_at)) }}</span>
						</div>
					</div>
				</div>
				@foreach ( $conversa->mensagens  as $m)
					@if ( $m->autor )
						{{-- expr --}}
						@if ( $m->autor->id == $usuario->id )
							<div class="col-xs-12 col-sm-10 corpo">
						@else
							<div class="col-xs-12 col-sm-10 col-sm-offset-2 corpo resposta">
						@endif
						<p class="mensagem-data">{{ $m->autor->nome_completo }} - {{ date( 'd/m/Y H:i', strtotime($m->created_at)) }}</p>

					@else
						<div class="col-xs-12 col-sm-10 col-sm-offset-2 corpo resposta">
						<p class="mensagem-data">Não identificado - {{ date( 'd/m/Y H:i', strtotime($m->created_at)) }}</p>
					@endif

						<p>{{ nl2br($m->texto) }}</p>
					</div>
				@endforeach

				{{ Form::open( [ "route" => "mensagens.update", "method" => "PUT" ] ) }}
					{{ Form::hidden('conversa', $conversa->id) }}

					@if ( $conversa->autor->id == $usuario->id  )
						{{ Form::hidden('from', $usuario->id) }}
						{{ Form::hidden('to', $conversa->destino->id) }}
					@else
						{{ Form::hidden('from', $usuario->id) }}
						{{ Form::hidden('to', $conversa->autor->id) }}
					@endif

				<div class="col-xs-12 form-group">
					{{ Form::textarea('mensagem', '', ['rows' => 5, 'class' => 'form-control'] ) }}
				</div>
				<div class="col-xs-12">
					<button class="btn btn-primary pull-right">Enviar resposta</button>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
@stop