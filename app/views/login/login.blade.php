@extends('master')

@section('content')
    <div class="box-login col-xs-12 col-sm-4 col-sm-offset-4">

        {{ Form::open(array('route' => 'login.validate', 'method' => 'POST')) }}
        
        @if ( $errors->has() )
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            @foreach( $errors->all('<p>:message</p>') as $message )
                {{ $message }}
            @endforeach
        </div>
        @endif

        <div class="form-group has-feedback">
            <input name="email" type="email" class="form-control" placeholder="Email" value="{{ Input::old("email") }}" />
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input name="senha" type="password" class="form-control" placeholder="Senha" value="{{ Input::old("senha") }}" />
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <label>
                    <input name="lembrar" value="1" type="checkbox" checked="checked"> Lembrar-se de mim
                </label>
                <a href="{{ route('login.esqueci') }}">Esqueci a senha</a>
            </div><!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
            </div><!-- /.col -->
            <div class="col-xs-12 text-center link-cadastro">
                <p><a href="{{ URL::route('cadastro') }}"><i class="glyphicon glyphicon-circle-arrow-right"></i> Ainda não está cadastrado? Clique aqui</a></p>
            </div>
        </div>

        {{ Form::close() }}
    </div>
@stop