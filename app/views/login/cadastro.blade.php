@extends('master')

@section('content')
    <div class="col-xs-12 col-sm-4 col-sm-offset-4">

        {{ Form::open(array('route' => 'cadastro.save', 'method' => 'POST')) }}
        {{ Form::token() }}

        @if ( $errors->has() )
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            @foreach( $errors->all('<p>:message</p>') as $message )
                {{ $message }}
            @endforeach
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="form-group has-feedback @if($errors->has('first_name')) has-error @endif">
                    <input name="first_name" type="text" class="form-control" placeholder="Nome" value="{{ Input::old("first_name") }}" />
                    @if($errors->has('first_name'))
                    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
                    @endif
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="form-group has-feedback @if($errors->has('last_name')) has-error @endif">
                    <input name="last_name" type="text" class="form-control" placeholder="Sobrenome" value="{{ Input::old("last_name") }}" />
                    @if($errors->has('last_name'))
                    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
                    @endif
                </div>
            </div>
        </div>

        <div class="form-group has-feedback @if($errors->has('email')) has-error @endif">
            <input name="email" type="email" class="form-control" placeholder="Email" value="{{ Input::old("email") }}" />
            @if($errors->has('email'))
            <span class="glyphicon glyphicon-remove form-control-feedback"></span>
            @endif
        </div>
        <div class="form-group has-feedback @if($errors->has('senha')) has-error @endif">
            <input name="senha" type="password" class="form-control" placeholder="Senha" value="{{ Input::old("senha") }}" />
            @if($errors->has('senha'))
            <span class="glyphicon glyphicon-remove form-control-feedback"></span>
            @endif
        </div>
        <div class="form-group has-feedback @if($errors->has('senha_confirmation')) has-error @endif">
            <input name="senha_confirmation" type="password" class="form-control" placeholder="Confirmar senha" value="" />
            @if($errors->has('senha_confirmation'))
            <span class="glyphicon glyphicon-remove form-control-feedback"></span>
            @endif
        </div>
        <div class="form-group has-feedback @if($errors->has('professor')) has-error @endif">
            {{ Form::select('professor', $professores, null, ["class" => "form-control"]) }}
            @if($errors->has('professor'))
            <span class="glyphicon glyphicon-remove form-control-feedback"></span>
            @endif
        </div>
        <div class="row">
            <div class="col-xs-8 link-cadastro">
                <p><a href="{{ URL::route('login') }}"><i class="glyphicon glyphicon-circle-arrow-right"></i> Já sou cadastrado.</a></p>
            </div>
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Cadastrar</button>
            </div>
        </div>

        {{ Form::close() }}
    </div>
@stop