@extends('master')

@section('content')
	<div class="box-login col-xs-12 col-sm-4 col-sm-offset-4">

		{{ Form::open(array('route' => 'login.atualiza', 'method' => 'POST')) }}
		{{ Form::token() }}

		{{ Form::hidden("id", Input::old("id", $id)) }}
		{{ Form::hidden("codigo", Input::old("codigo", $codigo)) }}

		@if ( $errors->has() )
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            @foreach( $errors->all('<p>:message</p>') as $message )
                {{ $message }}
            @endforeach
        </div>
        @endif

		<div class="form-group has-feedback">
			<input name="senha" type="password" class="form-control" placeholder="Senha" value="{{ Input::old('senha') }}" />
			<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		</div>

		<div class="form-group has-feedback">
			<input name="confirma_senha" type="password" class="form-control" placeholder="Confirme sua senha" value="{{ Input::old('confirma_senha') }}" />
			<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		</div>
		
		<div class="row">
			
			<div class="col-xs-12">
				<button type="submit" class="btn btn-primary btn-block btn-flat">Enviar informações para meu e-mail</button>
			</div><!-- /.col -->
		</div>

		{{ Form::close() }}
	</div>
@stop