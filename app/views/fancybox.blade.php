<link rel="stylesheet" type="text/css" href="{{ asset('/fancybox/jquery.fancybox.css?v=2.1.5') }}" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ asset('/fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7') }}" />
<script type="text/javascript" src="{{ asset('/js/vendor/jquery.mousewheel-3.0.6.pack.js') }}"></script>
<script type="text/javascript" src="{{ asset('/fancybox/jquery.fancybox.js?v=2.1.5') }}"></script>
<script type="text/javascript" src="{{ asset('/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
<script type="text/javascript" src="{{ asset('/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7') }}"></script>
<script type="text/javascript" src="{{ asset('/fancybox/helpers/jquery.fancybox-media.js?v=1.0.6') }}"></script>