@extends('master')

@section('content')
	<div class="videos row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			<h1 class="tit-area">Vídeos</h1>

			<div class="conteudo-container row">
				
				@if( $usuario->isProfessor() )
				<div class="col-xs-12">
					<a href="#formVideo" class="btn btn-primary btn-enviar-video fancybox">Enviar Vídeo</a>
				</div>
				@endif

				<div class="col-xs-12">
					<table class="tabela-videos row">
						<tr>
							<?php $i=1; ?>
							@foreach ($videos as $v)
								<td class="col-xs-6 col-sm-3" valign="top">
									<p>
										<a class="fancybox fancybox.iframe" href="http://youtube.com/v/{{ $v['yid'] }}">
											<img src="http://img.youtube.com/vi/{{ $v['yid'] }}/mqdefault.jpg" class="thumb-video">
											{{ str_limit($v['title'], 48, ' ...') }}
										</a><br>
										{{ $v['views'] }} Visualizações
									</p>

									@if( $usuario->isProfessor() )
									<p class="text-center"><a href="{{ route('videos.destroy', [$v['id']]) }}" class="btn-excluir-video">Excluir</a></p>
									@endif
								</td>

								@if ($i++ % 4 == 0)
									</tr><tr>
								@endif
							@endforeach

							@if ( $i < 4 )
								@while ( $i++ < 4 )
									<td></td>
								@endwhile
							@endif

					</table>
				</div>
				<div class="col-xs-12">
					{{ $query->links() }}
				</div>
			</div>
		</div>
	</div>

	@if( $usuario->isProfessor() )
	<div id="formVideo" class="esconde">
		{{ Form::open(array('url' => '/videos', 'method' => 'POST') )}}
		{{ Form::hidden('retorno', Route::getCurrentRoute()->getUri() ); }}
		<h2>URL do vídeo no Youtube:</h2>

		<div class="form-group">
			{{ Form::text('url', "", ['class' => 'form-control']) }}
		</div>

		<div class="form-group text-right">
			{{ Form::submit('Enviar', ['class' => 'btn btn-primary']); }}
			<button type="button" class="btn btn-primary btn-cancelar">Cancelar</button>
		</div>
		{{ Form::close() }}
	</div>
	@endif
@stop