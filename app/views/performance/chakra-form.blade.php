@extends('master')

@section('content')
	<div class="fotos row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			@include('usuarios.imagens')

			<h1 class="tit-area">Conhecimento</h1>
			<p>Os chakras coloridos representam o seu nível atual. Passe o mouse para uma breve descrição ou clique para ler a descrição completa do seu nível atual:</p>

			@if ( Session::has("message") )
				<div class="alert alert-info" role="alert">{{ Session::pull("message") }}</div>
			@endif

			@if ( $errors->has() )
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<i class="fa fa-ban"></i>
				@foreach( $errors->all('<p>:message</p>') as $message )
					{{ $message }}
				@endforeach
			</div>
			@endif

			<div class="conteudo-container row">
				<div class="col-xs-12 col-sm-4">
					<img src="{{ asset('img/chakras/c3.png') }}" alt="" width="100%">
				</div>
				<div class="col-xs-12 col-sm-8">
					{{ Form::model( $chakra, [ "route" => ["performance.chakra.update", $perfil->id, $chakra->id], "method" => "PUT" ] ) }}
					
					<div class="row">
						<div class="col-xs-12 form-group {{{ $errors->has('nome') ? 'has-error' : '' }}}">
							{{ Form::label("nome", "Nome") }}
							{{ Form::text("nome", null, array( "class" => "form-control" )) }}
						</div>
						<div class="col-xs-12 form-group {{{ $errors->has('resumo') ? 'has-error' : '' }}}">
							{{ Form::label("resumo", "Resumo") }}
							{{ Form::textarea("resumo", null, array( "class" => "form-control", "rows" => 2 )) }}
						</div>
						<div class="col-xs-12 form-group {{{ $errors->has('descricao') ? 'has-error' : '' }}}">
							{{ Form::label("descricao", "Descrição") }}
							{{ Form::textarea("descricao", null, array( "class" => "form-control", "rows" => 15 )) }}
						</div>

						<div class="col-xs-12">
							<button class="btn btn-primary btn-flat">SALVAR ALTERAÇÕES</button>
						</div>
					</div>

					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@stop