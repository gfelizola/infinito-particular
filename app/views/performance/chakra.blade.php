@extends('master')

@section('content')
	<div class="chakra-item row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			@include('usuarios.imagens')

			<h1 class="tit-area">Conhecimento</h1>
			<p>Os chakras coloridos representam o seu nível atual. Passe o mouse para uma breve descrição ou clique para ler a descrição completa do seu nível atual:</p>

			@if ( Session::has("message") )
				<div class="alert alert-info" role="alert">{{ Session::pull("message") }}</div>
			@endif

			<div class="conteudo-container row">
				<div class="col-xs-12 col-sm-4">
					<img src="{{ asset('img/chakras/c3.png') }}" alt="" width="100%">
				</div>
				<div class="col-xs-12 col-sm-8">
					<h3>{{ $chakra->nome }}</h3>
					<p>{{ nl2br( $chakra->descricao ) }}</p>
				</div>
				<div class="col-xs-12 text-right">
					
					<a href="{{ route('performance.aluno', [ $perfil->id ]) }}">< Voltar</a>
					@if ( $usuario->isAdmin() && $chakra->professor->id == $usuario->id )
						| <a href="{{ route('performance.chakra.edit', [ $perfil->id, $chakra->id ]) }}">Editar Chakra</a>
					@endif
				</div>
			</div>

			<div class="conteudo-container row lista-tecnicas">
				<div class="col-xs-12">
					<h2>Técnicas</h2>
					
					

					@if ( $usuario->isProfessor() )
					{{ Form::open(['route' => 'performance.chakra.tecAluno','method'=>'POST']) }}
					{{ Form::hidden('aid', $perfil->id ); }}
					{{ Form::hidden('retorno', Request::url() ); }}
					@endif

					@foreach ($tecnicas as $tec)
						@if ( count( $tec["items"] ) )
							<p><b>{{ $tec["nome"] }}</b></p>

							<ul class="">
								@foreach ($tec["items"] as $tec)
									<?php
									$c = "";
									$ch = "";
									if( $perfil->tecnicas()->find( $tec->id ) ){
										$c = "feita";
										$ch = "checked='checked'";
									}
									?>
									<li class="{{ $c }}">
										<label for="tec_{{ $tec->id }}">
											@if ( $usuario->isProfessor() )
											<input type="checkbox" {{ $ch }} name="tecnicas[]" id="tec_{{ $tec->id }}" value="{{ $tec->id }}" class="check-tecnica" />
											@endif
											{{ $tec->nome }}
										</label>
									</li>
								@endforeach
							</ul>
						@endif
					@endforeach

					@if ( $usuario->isProfessor() )
					{{ Form::submit('Salvar', ['class' => 'btn btn-primary']); }}
					{{ Form::close() }}
					@endif

					@if ( $usuario->isAdmin() )
					<a href="#formTecnica" class="btn btn-default btn-toggle">Adiciona técnica</a>
					@endif
				</div>
			</div>

			@if ( $usuario->isAdmin() )

				@if ( $errors->has() )
					<div class="col-xs-12">
						<div class="alert alert-danger alert-dismissable">
							<p><i class="fa fa-ban"></i> <b>Aviso!</b></p>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							@foreach( $errors->all('<p>:message</p>') as $message )
								{{ $message }}
							@endforeach
						</div>
					</div>
					<div id="formTecnica" class="conteudo-container row">
				@else
					<div id="formTecnica" class="conteudo-container esconde row">
				@endif

				
					<div class="col-xs-12">
						<h2>Criar nova técnica para este chakra:</h2>
					</div>
					
					{{ Form::open(['route' => ['performance.chakra.tecnicac', $chakra->id ]]) }}
					{{ Form::hidden('retorno', Request::url() ); }}
					{{ Form::hidden('cid', $chakra->id ); }}
					
					<div class="col-xs-12 form-group {{{ $errors->has('nome') ? 'has-error' : '' }}}">
						{{ Form::label("nome", "Nome") }}
						{{ Form::text("nome", Input::old('nome'), array( "class" => "form-control" )) }}
					</div>
					<div class="col-xs-12 form-group {{{ $errors->has('tipo') ? 'has-error' : '' }}}">
						{{ Form::label("tipo", "Tipo") }}
						{{ Form::select('tipo', [
							"1" => "Ásanas",
							"2" => "Respiração",
							"3" => "Meditação",
							"4" => "Limpeza",
						], Input::old('tipo'), array( "class" => "form-control" ) ); }}
					</div>
					
					<div class="col-xs-12 form-group">
						{{ Form::submit('Enviar', ['class' => 'btn btn-primary']); }}
					</div>
					{{ Form::close() }}
					
				</div>

			@endif
		</div>
	</div>
@stop