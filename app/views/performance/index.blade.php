@extends('master')

@section('content')
	<div class="fotos row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			@include('usuarios.imagens')

			<h1 class="tit-area">Conhecimento</h1>
			<p>Os chakras coloridos representam o seu nível atual. Passe o mouse para uma breve descrição ou clique para ler a descrição completa do seu nível atual:</p>

			@if ( Session::has("message") )
				<div class="alert alert-info" role="alert">{{ Session::pull("message") }}</div>
			@endif

			<div class="conteudo-container row">
				<div class="col-xs-12 col-sm-6 col-sm-offset-3">
					<div class="chakras">
						@for ($i = 7; $i > 0; $i--)
							<?php $c = $chakras[$i-1] ?>
							
							@if ( $i <= $perfil->chakra )
								<a href="{{ route('performance.chakra', [$perfil->id, $c->id]) }}" class="chakra chakra-{{ $i }} ativo">
									<span class="img-chakra"></span>
									<span class="resumo">{{ $c->resumo }}</span>
								</a>
							@else
								<span class="chakra chakra-{{ $i }}">
									<span class="img-chakra"></span>
									<span class="resumo">{{ $c->resumo }}</span>
								</span>
							@endif
						@endfor
					</div>
				</div>

				@if ( $usuario->isProfessor() )
					{{ Form::open(['route' => ['performance.alunoUpdate', $perfil->id], 'method' => 'POST']) }}
					<div class="col-xs-12 col-sm-3 form-group">
						{{ Form::label('nivel', 'Trocar nível do aluno:') }}
						{{ Form::select('nivel', [
							"1" => "1",
							"2" => "2",
							"3" => "3",
							"4" => "4",
							"5" => "5",
							"6" => "6",
							"7" => "7",
						], $perfil->chakra, ['class' => 'form-control']) }}
						<button class="btn btn-primary btn-block">Salvar</button>
					</div>
					{{ Form::close() }}
				@endif
			</div>
		</div>
	</div>
@stop