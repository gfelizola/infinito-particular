@extends('master-lightbox')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-offset-2 text-center">
				<h1>Página não encontrada</h1>
			</div>
		</div>
	</div>
@stop