<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		//CRIAR USUÁRIOS E GRUPOS
		//============================================

		//Grupo admin
		try
		{
			$gAdmin = Sentry::findGroupByName('admin');
			$this->command->info('Grupo admin já existe');
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
			$gAdmin = Sentry::createGroup(array(
				'name'         => 'admin',
				'permissions'  => array(
					'admin'    => 1,
					'users'    => 1,
					'aluno'    => 1,
				),
			));
			$this->command->info('Grupo admin criado com sucesso');
		}

		//Grupo professor
		try
		{
			$gProf = Sentry::findGroupByName('professor');
			$this->command->info('Grupo professor já existe');
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
			$gProf = Sentry::createGroup(array(
				'name'         => 'professor',
				'permissions'  => array(
					'admin'    => 0,
					'users'    => 0,
					'aluno'    => 1,
				),
			));
			$this->command->info('Grupo professor criado com sucesso');
		}

		//Grupo aluno
		try
		{
			$gAluno = Sentry::findGroupByName('aluno');
			$this->command->info('Grupo aluno já existe');
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
			$gAluno = Sentry::createGroup(array(
				'name'         => 'aluno',
				'permissions'  => array(
					'admin'    => 0,
					'users'    => 0,
					'aluno'    => 0,
				),
			));
			$this->command->info('Grupo aluno criado com sucesso');
		}

		try
		{
			$user1 = Sentry::createUser(array(
				'email'     => 'gfelizola@gmail.com',
				'password'  => 'gustavof',
				'first_name'  => 'Gustavo',
				'last_name'  => 'Felizola',
				'activated' => true,
			));

			$user1->addGroup($gAdmin);
			$this->command->info('Usuario 1 gfelizola criado com sucesso');
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
			$user1 = Sentry::findUserByCredentials(array(
				'email' => 'gfelizola@gmail.com',
			));
			$this->command->info('Usuario 1 gfelizola já existe');
		}

		try
		{
			$user2 = Sentry::createUser(array(
				'email'     => 'admin@infinito.com',
				'password'  => '1infinit0',
				'first_name'  => 'Admin',
				'last_name'  => 'Teste',
				'activated' => true,
			));

			$user2->addGroup($gAdmin);
			$this->command->info('Usuario 2 infinito criado com sucesso');
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
			$user2 = Sentry::findUserByCredentials(array(
				'email' => 'admin@infinito.com',
			));
			$this->command->info('Usuario 2 (Admin) já existe');
		}

		try
		{
			$user3 = Sentry::createUser(array(
				'email'     => 'professor@infinito.com',
				'password'  => '1infinit0',
				'first_name'  => 'Tcher',
				'last_name'  => 'Um',
				'activated' => true,
			));

			$user3->addGroup($gProf);
			$this->command->info('Usuario 3 (Professor) criado com sucesso');
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
			$user3 = Sentry::findUserByCredentials(array(
				'email' => 'professor@infinito.com',
			));
			$this->command->info('Usuario 3 (Professor) já existe');
		}

		try
		{
			$user4 = Sentry::createUser(array(
				'email'     => 'aluno@infinito.com',
				'password'  => '1infinit0',
				'first_name'  => 'Luno',
				'last_name'  => 'Dois',
				'activated' => true,
			));

			$user4->addGroup($gAluno);
			$this->command->info('Usuario 4 (Aluno) criado com sucesso');
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
			$user4 = Sentry::findUserByCredentials(array(
				'email' => 'aluno@infinito.com',
			));
			$this->command->info('Usuario 4 (Aluno) já existe');
		}


		//CRIANDO VÍDEOS
		//========================================

		$urls = [
			"https://www.youtube.com/watch?v=d8eU4qbzR5w",
			"https://www.youtube.com/watch?v=tXeqYKdTjwU",
			"https://www.youtube.com/watch?v=6YVnHLaoO4A",
			"https://www.youtube.com/watch?v=T2FRasQK0R0",
			"https://www.youtube.com/watch?v=z7SWbqdGSYs",
			"https://www.youtube.com/watch?v=OUrQCQxYHWI"
		];

		foreach ($urls as $u) {
			$video = Video::create([
				"url" => $u,
				"nome" => uniqid()
			]);

			$video->users()->attach( $user1->id );
		}

		$this->command->info('Todos videos criados');

		//CRIANDO FOTOS E ALBUMS
		//=========================================

		$album = new Album(["nome" => "Album bonito"]);
		$album->user()->associate( $user1 );
		$album->save();

		for ($i=1; $i <= 9; $i++) {
			$path = "/uploads/foto$i.jpg";

			$foto = new Foto([
				"nome" => "Foto $i",
				"path" => $path
			]);

			$foto->album()->associate($album);
			$foto->save();
		}

		$this->command->info('Album e fotos criadas');

		//CRIANDO MENSAGENS E CONVERSAS
		//=========================================

		$conversa = new Conversa();
		$conversa->destino()->associate( $user1 );
		$conversa->autor()->associate( $user2 );
		$conversa->save();

		$mensagens = [
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eros diam, blandit eget porta viverra, lacinia vel turpis. Aliquam erat volutpat. Vivamus commodo nunc arcu, ut eleifend nulla congue vitae. Nullam in urna eros. Nullam maximus est id congue malesuada. Duis lacus dui, tincidunt sed diam in, dictum sollicitudin felis. Vestibulum accumsan, nisi et egestas venenatis, massa massa lacinia enim, sit amet cursus libero odio et diam.",
			"Vestibulum luctus posuere dolor, quis luctus justo accumsan a. Vivamus non interdum turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Integer blandit nulla ut sapien varius, non mollis odio ullamcorper. Nulla vitae pulvinar nibh. Aliquam erat volutpat. Suspendisse potenti. In velit nisl, pulvinar sit amet massa et, iaculis faucibus ipsum. Nam et elit et elit finibus suscipit. Donec rutrum tincidunt justo, quis fermentum ipsum aliquet ut. Curabitur vitae dui tincidunt, volutpat lectus nec, mattis lacus. Nunc id massa auctor, facilisis libero non, pulvinar eros. Praesent commodo, velit ac posuere lacinia, metus arcu luctus mi, quis pellentesque leo massa ut tortor.",
			"Integer tempor dictum consequat. Mauris et mi augue. Nulla et erat eu libero eleifend molestie sodales a nibh. Suspendisse diam felis, bibendum id suscipit vel, auctor sit amet purus. Suspendisse vitae consequat nulla. Nunc tempus commodo ligula. Nam pretium turpis justo, at feugiat quam auctor eget. In pretium libero at mauris luctus semper. Donec ligula lacus, molestie sit amet metus suscipit, tempor viverra ipsum. Donec laoreet erat sem, in convallis dui semper cursus. Phasellus rhoncus mauris eu quam vulputate, eget aliquam tortor suscipit.",
			"Quisque aliquet justo sed magna sagittis auctor. Nullam sit amet consectetur quam. Curabitur eu nulla tortor. Aenean at lorem aliquam, vehicula neque vel, elementum augue. Morbi at lacus augue. Duis hendrerit ac ipsum vitae imperdiet. Praesent magna orci, tristique vel diam eu, porttitor iaculis eros. Sed pulvinar vehicula semper. Nullam lobortis enim at arcu sollicitudin, quis congue nisl maximus. Pellentesque id felis vitae justo blandit convallis. In sed dapibus dui. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec eleifend tellus ut sapien porta, vel pellentesque leo fermentum. Phasellus sodales, nisi quis pulvinar pretium, enim lectus molestie libero, non rhoncus justo arcu eu quam. Vestibulum et purus ut magna rhoncus auctor. Cras vulputate nisl et egestas aliquet.",
			"Maecenas pulvinar tincidunt magna, quis blandit urna mollis sit amet. Vivamus tempus sit amet metus in molestie. Aenean congue porta magna. Fusce scelerisque, mi et porttitor varius, urna dui accumsan dolor, at tristique odio justo vitae nibh. Quisque malesuada dignissim eros at eleifend. Donec eget eros sed est consequat cursus id vel turpis. In ultricies dolor ac massa tincidunt finibus. Donec tempus lorem elit, in accumsan elit aliquet at. Curabitur molestie arcu eget odio facilisis mattis. Donec faucibus justo vel ligula hendrerit ultricies. Fusce iaculis iaculis ligula id blandit. Donec aliquam mauris eget magna consectetur, nec volutpat ex vulputate. Vestibulum consequat, nisl a suscipit fermentum, ante ex vulputate nibh, vel dignissim nulla odio eget massa.",
		];

		$c = 1;
		foreach ($mensagens as $m) {
			$msg = new Mensagem([
				"assunto" => "Assunto $c",
				"texto" => $m
			]);

			$msg->conversa()->associate($conversa);
			$msg->save();
			$c++;
		}

		$conversa2 = new Conversa();
		$conversa2->destino()->associate( $user2 );
		$conversa2->autor()->associate( $user1 );
		$conversa2->save();

		$c = 1;
		foreach ($mensagens as $m) {
			$msg = new Mensagem([
				"assunto" => "Assunto $c",
				"texto" => $m
			]);

			$msg->conversa()->associate($conversa);
			$msg->save();
			$conversa2->mensagens()->save($msg);
			$c++;
		}

		$this->command->info('Conversas e mensagens criadas');

		//CRIANDO CHAKRAS
		//=========================================

		for ($i=1; $i <=7; $i++) { 
			$chakra = Chakra::create([
				"nome" => "Chakra n$i",
				"resumo" => "Este é o chakra Chakra n$i",
				"ordem" => $i,
				"descricao" => "C$i Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro vero consequatur quia, nobis omnis vitae in repellat culpa ab expedita facere odit sequi, quae, ducimus voluptates architecto commodi numquam ipsum?",
			]);

			$chakra->professor()->associate( $user3 );
			$chakra->save();
		}

		
	}

}

class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//cria os grupos de usuários se não existirem



	}

}