<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetasNotas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('metas_notas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->string('descricao')->nullable();
			$table->string('descricao_valor')->nullable();
			$table->integer('valor')->default(0);
			$table->integer('meta_id')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('metas_notas');
	}

}
