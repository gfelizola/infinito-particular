<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsAutorOnMensagem extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mensagens', function(Blueprint $table)
		{
			$table->integer('autor_id')->unsigned()->default(0);;
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mensagens', function(Blueprint $table)
		{
			$table->dropColumn('autor_id');
		});
	}

}
