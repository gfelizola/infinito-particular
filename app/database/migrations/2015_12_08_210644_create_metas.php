<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('metas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->string( 'nome');
			$table->string( 'meta');
			// $table->string( 'descricao')->nullable();
			// $table->integer('percentual')->default(0);
			// $table->string( 'descricao_percentual');
			$table->string( 'video')->nullable();
			$table->string( 'foto')->nullable();
			$table->integer('tipo')->default(0);
			$table->integer('aluno_id')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('metas');
	}

}
