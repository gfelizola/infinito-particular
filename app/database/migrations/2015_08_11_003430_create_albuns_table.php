<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlbunsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('albuns', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->string('nome');
			$table->integer('user_id')->unsigned();
		});

		Schema::create('albuns_users', function($table)
		{
			$table->integer('user_id')->unsigned();
			$table->integer('album_id')->unsigned();
			$table->engine = 'InnoDB';
			$table->primary(array('user_id', 'album_id'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('albuns');
		Schema::drop('albuns_users');
	}

}
