<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTecnicas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tecnicas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->string('nome');
			$table->integer('chakra_id')->default(0);
			$table->integer('tipo')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tecnicas');
	}

}
