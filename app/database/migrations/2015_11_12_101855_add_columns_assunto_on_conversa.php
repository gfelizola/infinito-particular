<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsAssuntoOnConversa extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('conversas', function(Blueprint $table)
		{
			$table->string('assunto')->default("Sem assunto");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('conversas', function(Blueprint $table)
		{
			$table->dropColumn('assunto');
		});
	}

}
