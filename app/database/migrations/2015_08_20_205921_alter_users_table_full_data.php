<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTableFullData extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->softDeletes();

			$table->string('peso')->nullable();
			$table->string('altura')->nullable();
			$table->char('sexo')->nullable();
			$table->string('profissao')->nullable();
			$table->string('estado_civil')->nullable();
			$table->string('endereco')->nullable();
			$table->string('telefone')->nullable();
			$table->string('nacionalidade')->nullable();
			$table->string('objetivos')->nullable();
			$table->string('facebook')->nullable();
			$table->string('twitter')->nullable();
			$table->boolean('aprovado')->nullable();
			$table->boolean('cadastro_completo')->default(false);
			$table->char('status')->nullable();
			$table->date('data_nascimento')->nullable();
			$table->date('inicio_infinito')->nullable();
			$table->date('inicio_yoga')->nullable();
			$table->text('observacoes')->nullable();
			$table->string('dados_publicos')->default('*');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropColumn(array(
				'sexo',
				'peso',
				'altura',
				'profissao',
				'estado_civil',
				'endereco',
				'telefone',
				'nacionalidade',
				'objetivos',
				'facebook',
				'twitter',
				'aprovado',
				'cadastro_completo',
				'status',
				'data_nascimento',
				'inicio_infinito',
				'inicio_yoga',
				'observacoes',
				'dados_publicos',
			));

			$table->dropSoftDeletes();
		});
	}

}
