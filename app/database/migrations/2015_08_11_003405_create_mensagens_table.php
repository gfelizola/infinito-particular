<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMensagensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mensagens', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->integer('conversa_id')->unsigned()->default(0);
			$table->dateTime('enviado_at')->nullable();
			$table->string('assunto')->nullable();
			$table->text('texto');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mensagens');
	}

}
