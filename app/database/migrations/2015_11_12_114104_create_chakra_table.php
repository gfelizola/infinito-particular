<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChakraTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('chakras', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->integer('professor_id')->default(0);
			$table->integer('ordem')->default(0);
			$table->string('nome');
			$table->string('resumo',255);
			$table->text('descricao');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('chakras');
	}

}
