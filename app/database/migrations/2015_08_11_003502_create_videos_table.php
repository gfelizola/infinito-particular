<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('videos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->string('nome')->nullable();
			$table->string('url');
		});

		Schema::create('videos_users', function($table)
		{
			$table->integer('user_id')->unsigned();
			$table->integer('video_id')->unsigned();
			$table->engine = 'InnoDB';
			$table->primary(array('user_id', 'video_id'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('videos');
		Schema::drop('videos_users');
	}

}
