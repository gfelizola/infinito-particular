<?php

class MetasController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return $this->aluno( $this->user->id );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function aluno($aid)
	{
		$aluno = User::find($aid);

		$metas = $aluno->metas;

		$posicoes = [];
		$respiracoes = [];
		$meditacoes = [];
		$limpezas = [];

		if ( count( $metas ) ) {
			$posicoes = $metas->filter( function($item){
				return $this->filtraMeta( $item , Meta::TIPO_ASANA );
			});

			$respiracoes = $metas->filter( function($item){
				return $this->filtraMeta( $item , Meta::TIPO_RESPIRACAO );
			});

			$meditacoes = $metas->filter( function($item){
				return $this->filtraMeta( $item , Meta::TIPO_MEDITACAO );
			});

			$limpezas = $metas->filter( function($item){
				return $this->filtraMeta( $item , Meta::TIPO_LIMPEZA );
			});
		}

		return Response::view('metas.index', [
			"aluno"       => $aluno,
			"posicoes"    => $posicoes,
			"respiracoes" => $respiracoes,
			"meditacoes"  => $meditacoes,
			"limpezas"    => $limpezas,
		]);
	}

	private function filtraMeta($meta, $tipo)
	{
		return $meta->tipo == $tipo;
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($tipo, $aid)
	{
		return Response::view('metas.form');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), Meta::$rules);
		$tipo = Input::get('tipo');

		if ($validator->fails())
		{
			return Redirect::route("metas.criar", [ $tipo, Input::get('aid') ])->withErrors($validator)->withInput( Input::all() );
		}
		else
		{
			// dd("Dados gravados");

			$meta = Meta::create([
				"nome"  => Input::get("nome"),
				"meta"  => Input::get("meta"),
				"video" => Input::get("video"),
				"tipo"  => $tipo,
			]);

			$aluno = User::find( Input::get("aid") );

			if ( Input::hasFile('foto') ) {
				$foto     = Input::file('foto');
				$destino  = Config::get("infinito.UPLOAD_PATH") . "/metas";
				$fileName = 'meta_' . $tipo . "_" . $aluno->id . "_" . $meta->id . "." . $foto->getClientOriginalExtension();
				$foto->move($destino, $fileName);

				$meta->foto = $fileName;
			}

			$meta->aluno()->associate($aluno);
			$meta->save();

			$this->createHistorico(
				"Nova meta! <a href='" . route("metas.aluno", [$aluno->id]) . "'>veja mais</a>",
				"heart",
				$aluno
			);

			return Redirect::route("metas.aluno", [ Input::get('aid') ])->with("message","Meta criada com sucesso");
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function addNota()
	{
		$validator = Validator::make(Input::all(), MetaNota::$rules);
		
		if ($validator->fails())
		{
			return Redirect::route("metas.show", [ Input::get('meta') ])->withErrors($validator)->withInput( Input::all() );
		}
		else
		{
			// dd("Dados gravados");

			$nota = MetaNota::create([
				"descricao"  => Input::get("descricao"),
				"valor"  => Input::get("valor"),
			]);

			$meta = Meta::find( Input::get("meta") );
			$meta->notas()->save($nota);
			$nota->meta()->associate($meta);

			$meta->save();
			$nota->save();

			$this->createHistorico(
				"Nova anotação em meta! <a href='" . route("metas.aluno", [$meta->aluno->id]) . "'>veja mais</a>",
				"flag",
				$meta->aluno
			);

			return Redirect::route("metas.show", [ $meta->id ])->with("message_nota","Nota adicionada com sucesso");
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function addFoto()
	{
		$validator = Validator::make(Input::all(), MetaFoto::$rules);

		if ($validator->fails())
		{
			return Redirect::route("metas.show", [ Input::get('meta') ])->withErrors($validator)->withInput( Input::all() );
		}
		else
		{
			$meta = Meta::find( Input::get("meta") );

			$metaFoto = MetaFoto::create([
				"url"  => uniqid()
			]);

			$foto     = Input::file('imagem');
			$destino  = Config::get("infinito.UPLOAD_PATH") . "/metas/meta_" . $meta->id;
			$fileName = 'meta_' . $metaFoto->id . "." . $foto->getClientOriginalExtension();
			$foto->move($destino, $fileName);

			$metaFoto->url = $fileName;
			$metaFoto->save();

			$meta->fotos()->save($metaFoto);
			$metaFoto->meta()->associate($meta);

			$meta->save();
			$metaFoto->save();

			$this->createHistorico(
				"Nova foto de meta! <a href='" . route("metas.aluno", [$meta->aluno->id]) . "'>veja aqui</a>",
				"picture",
				$meta->aluno
			);

			return Redirect::route("metas.show", [ $meta->id ])->with("message_foto","Foto adicionada com sucesso");
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$meta = Meta::findOrFail($id);

		$fotos = $meta->fotos->sortByDesc('created_at');
		$notas = $meta->notas->sortByDesc('created_at');

		// dd($notas->first());

		return Response::view('metas.item', [
			"meta"  => $meta,
			"notas" => $notas,
			"fotos" => $fotos,
		]);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return Response::view('metas.form', [
			"meta" => Meta::find($id)
		]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$validator = Validator::make(Input::all(), Meta::$rules);
		$tipo = Input::get('tipo');

		if ($validator->fails())
		{
			return Redirect::route("metas.criar", [ $tipo, Input::get('aid') ])->withErrors($validator)->withInput( Input::all() );
		}
		else
		{
			$meta = Meta::find($id);
			// dd([$id, $meta]);
			$meta->fill([
				"nome"  => Input::get("nome"),
				"meta"  => Input::get("meta"),
				"video" => Input::get("video"),
			]);

			if ( Input::hasFile('foto') ) {
				$foto     = Input::file('foto');
				$destino  = Config::get("infinito.UPLOAD_PATH") . "/metas";
				$fileName = 'meta_' . $tipo . "_" . $aluno->id . "_" . $meta->id . "." . $foto->getClientOriginalExtension();
				$foto->move($destino, $fileName);

				$meta->foto = $fileName;
			}

			$meta->save();

			return Redirect::route("metas.aluno", [ Input::get('aid') ])->with("message","Meta alterada com sucesso");
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
