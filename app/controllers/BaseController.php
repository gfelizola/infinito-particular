<?php

class BaseController extends Controller {

	protected $user;

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}

		$rota = Route::getCurrentRoute();
		$path = $rota->uri();
		$nome = $rota->getName();
		$url  = Request::url();

		$nome = str_replace('.', '-', $nome);
		View::share('mainClass', $nome);

		$this->user = Sentry::getUser();
		View::share("usuario", $this->user);
		View::share("perfil", $this->user);

		$menu = [
			"home" => [
				"url" => route('home'),
				"nome" => "Home",
				"ativo" => route('home') == $url,
			],
			"perfil" => [
				"url" => route('usuarios.show', $this->user ? $this->user->id : null),
				"nome" => "Meu Perfil",
				"ativo" => route('usuarios.show', $this->user ? $this->user->id : null) == $url,
			],
			"usuarios.index" => [
				"url" => route('usuarios.index'),
				"nome" => "Quem faz parte do infinito",
				"ativo" => route('usuarios.index') == $url,
			],
			"performance" => [
				"url" => route('performance.aluno', $this->user ? $this->user->id : null),
				"nome" => "Conhecimento",
				"ativo" => route('performance.index') == $url,
			],
			"metas" => [
				"url" => route('metas.index'),
				"nome" => "Performance",
				"ativo" => route('metas.index') == $url,
			],
			"fotos" => [
				"url" => route('fotos.index'),
				"nome" => "Fotos",
				"ativo" => route('fotos.index') == $url,
			],
			"videos" => [
				"url" => route('videos.index'),
				"nome" => "Vídeos",
				"ativo" => route('videos.index') == $url,
			],
			"mensagens" => [
				"url" => route('mensagens.index'),
				"nome" => "Mensagens",
				"ativo" => route('mensagens.index') == $url,
			],
			"logout" => [
				"url" => route('logout'),
				"nome" => "Sair",
				"ativo" => route('logout') == $url,
			],
		];

		if ( ! is_null($this->user) && $this->user->isProfessor() ) {
			//menu professor
			unset($menu['performance']);
			unset($menu['metas']);
			$menu['usuarios.index']['nome'] = "Meus alunos";
			$menu['usuarios.index']['url'] = $menu['usuarios.index']['url'] . "?p=" . $this->user->id;
		}


		// foreach ($menu as $k => $item) {
		// 	$item["ativo"] = $item["url"] == $url ? "active" : "";
		// }
		View::share("menu", $menu);

		if ( $this->user ) {
			$eventos = [];
			$tipo = ['','event-important','event-success','event-warning','event-info','event-inverse','event-special'];

			foreach ($this->user->eventos as $e) {
				$eventos[] = [
					"id"        => $e->id,
					"title"     => $e->nome,
					"class"     => $tipo[$e->tipo],
					"data"      => $e->data_evento->format('d/m/Y'),
					"url"       => route('eventos.show', [$e->id]),
					"start"     => $e->data_evento->timestamp * 1000,
					"end"       => $e->data_evento->timestamp * 1000,
					"descricao" => $e->descricao,
				];
			}

			View::share("eventos", $eventos);
		}
	}

	public function createHistorico($m, $i, $u)
	{
		$h = Historico::create([
			"mensagem" => $m,
			"icone" => $i
		]);

		$h->user()->associate($u);
		$h->save();
	}

}
