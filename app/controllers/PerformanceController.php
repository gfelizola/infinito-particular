<?php

class PerformanceController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id = NULL)
	{
		
		return Response::view("performance.index");
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function aluno($id)
	{
		$perfil    = User::find($id);

		if ( $perfil->isProfessor() ) {
			$chakras   = $perfil->chakras->sortByDesc('ordem');
		} else {
			$professor = $perfil->professor;
			$chakras   = $professor->chakras->sortByDesc('ordem');
		}

		if ( $perfil ) {
			return Response::view("performance.index",[
				"perfil" => $perfil,
				"chakras" => $chakras
			]);
		} else {
			App::abort(404);
		}

	}

	public function alunoUpdate($id)
	{
		$perfil = User::find($id);
		$nivel = Input::get('nivel');

		if ( $perfil ) {
			$perfil->chakra = $nivel;
			$perfil->save();

			$this->createHistorico(
				"Parabéns! <a href='" . route('performance.aluno', [$id]) . "'>Novo nível de conhecimento</a>",
				"star",
				$perfil
			);

			return Redirect::route('performance.aluno',[$perfil->id])->with("message","Nível atualizado com sucesso");
		} else {
			App::abort(404);
		}

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($aid, $cid)
	{
		$aluno  = User::find($aid);
		$chakra = Chakra::find($cid);

		if ( ! $chakra ) {
			return Redirect::route('performance.aluno', [$aid])->with("message","Chakra não encontrado");
		}

		$tecs       = $chakra->tecnicas;

		$posicoes    = [];
		$respiracoes = [];
		$meditacoes  = [];
		$limpezas    = [];

		if ( count( $tecs ) ) {
			$posicoes = $tecs->filter( function($item){
				return $this->filtraMeta( $item , Meta::TIPO_ASANA );
			});

			$respiracoes = $tecs->filter( function($item){
				return $this->filtraMeta( $item , Meta::TIPO_RESPIRACAO );
			});

			$meditacoes = $tecs->filter( function($item){
				return $this->filtraMeta( $item , Meta::TIPO_MEDITACAO );
			});

			$limpezas = $tecs->filter( function($item){
				return $this->filtraMeta( $item , Meta::TIPO_LIMPEZA );
			});
		}

		$tecnicas = [
			"posicoes"    => [
				"nome" => "Ásanas",
				"items" => $posicoes
			],
			"respiracoes"    => [
				"nome" => "Respiração",
				"items" => $respiracoes
			],
			"meditacoes"    => [
				"nome" => "Meditação",
				"items" => $meditacoes
			],
			"limpezas"    => [
				"nome" => "Limpeza",
				"items" => $limpezas
			],
		];

		return Response::view('performance.chakra', [
			"perfil"   => $aluno,
			"chakra"   => $chakra,
			"tecnicas" => $tecnicas,
		]);
	}

	private function filtraMeta($meta, $tipo)
	{
		return $meta->tipo == $tipo;
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function tecAluno()
	{
		$aluno = User::findOrFail( Input::get('aid') );

		// dd(  );

		$inp_tecs = Input::get('tecnicas');
		$tecnicas = [];

		foreach ($inp_tecs as $key => $value) {
			array_push($tecnicas, intval( $value ));
		}

		$aluno->tecnicas()->sync($tecnicas);

		$this->createHistorico(
			"Parabéns! <a href='" . Input::get('retorno') . "'>Novas técnicas realizadas</a>",
			"star",
			$aluno
		);

		return Redirect::to( Input::get('retorno') )->with("message","Tecnicas salvas com sucesso");

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($aid, $id)
	{
		$chakra = Chakra::find($id);

		if ( ! $chakra ) {
			return Redirect::route('performance.aluno', [$this->user->id])->with("message","Chakra não encontrado");
		}

		return Response::view('performance.chakra-form', [
			"chakra" => $chakra,
			"aid" => $aid,
		]);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function createTecnica($cid)
	{
		// dd($cid);

		$validator = Validator::make(Input::all(), Tecnica::$rules);

		if ($validator->fails())
		{
			return Redirect::to( Input::get('retorno') )->withErrors($validator)->withInput( Input::all() );
		}
		else
		{
			// dd("Dados gravados");

			$tec = Tecnica::create( Input::all() );
			$chakra = Chakra::find( Input::get("cid") );

			$tec->chakra()->associate($chakra);
			$tec->save();

			return Redirect::to( Input::get('retorno') )->with("message","Tecnica criada com sucesso");
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($aid, $id)
	{
		$c = Chakra::find($id);

		if ( $c ) {

			$validator = Validator::make(Input::all(), Chakra::$rules);

			if ($validator->fails()) {
				return Redirect::route("performance.chakra.edit", [$aid, $c->id])->withErrors($validator)->withInput( Input::all() );
			} else {

				$c->fill( Input::all() );
				$c->save();

				return Redirect::route("performance.chakra", [$aid, $c->id])->with("message","Dados atualizados com sucesso");
			}
		} else {
			App::abort(404);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
