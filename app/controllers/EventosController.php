<?php

use \Carbon\Carbon;

class EventosController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Response::view('fotos.index',[
			'albuns' => $albuns
		]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function calendar($aid = null)
	{
		$aluno = User::find($aid);

		$eventos = [];
		$tipo = ['','event-important','event-success','event-warning','event-info','event-inverse','event-special'];

		foreach ($aluno->eventos as $e) {
			$eventos[] = [
				"id"        => $e->id,
				"title"     => $e->nome,
				"class"     => $tipo[$e->tipo],
				"data"      => $e->data_evento,
				"url"       => route('eventos.show', [$e->id]),
				"start"     => $e->data_evento->timestamp * 1000,
				"end"       => $e->data_evento->timestamp * 1000,
				"descricao" => $e->descricao,
			];
		}

		// dd($eventos);

		return Response::view('eventos.calendario', [
			"aid" => $aid,
			"eventos" => json_encode( $eventos )
		]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($aid)
	{
		return Response::view('eventos.form', [
			"aid"    => $aid,
			"evento" => null,
		]);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), Evento::$rules);
		$aid = Input::get('aluno');

		if ($validator->fails())
		{
			return Redirect::route("eventos.createc", [ $aid ])->withErrors($validator)->withInput( Input::all() );
		}
		else
		{
			$aluno  = User::find( $aid );
			$evento = Evento::create( Input::all() );
			
			$evento->aluno()->associate( $aluno );
			$evento->save();

			$this->createHistorico(
				"Novo evento! <a href='" . route("eventos.calendar", [ $aid ]) . "'>" . $evento->nome . "</a>",
				"calendar",
				$aluno
			);

			return Redirect::route("eventos.calendar", [ $aid ])->with("message", "Evento criado com sucesso");
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		Evento::find($id);


	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($aid, $id)
	{
		$evento = Evento::find($id);

		// dd( $evento->toArray() );

		if ( $evento ) {
			return Response::view('eventos.form', [
				"aid"    => $aid,
				"evento" => $evento
			]);
		} else {
			App::abort(404);
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$validator = Validator::make(Input::all(), Evento::$rules);
		$aid = Input::get('aluno');

		if ($validator->fails())
		{
			return Redirect::route("eventos.createc", [ $aid ])->withErrors($validator)->withInput( Input::all() );
		}
		else
		{
			$evento = Evento::find($id);
			$evento->fill( Input::all() );
			$evento->save();

			return Redirect::route("eventos.calendar", [ $aid ])->with("message", "Evento atualizado com sucesso");
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		Evento::destroy($id);
		return Response::make(json_encode(["sucesso" => true]));
	}


}
