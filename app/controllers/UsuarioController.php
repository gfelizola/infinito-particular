<?php

class UsuarioController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$professor = Input::get('p');
		$busca = Input::get('q');

		if ( $professor && ! empty( $professor ) ) {
			$usuarios = User::where('professor_id','=', $professor)->orderBy('first_name','ASC')->orderBy('last_name','ASC');
			# code...
		} else {
			$usuarios = User::orderBy('first_name','ASC')->orderBy('last_name','ASC');
		}

		if ( $busca && ! empty( $busca ) ) {
			$usuarios = $usuarios->where('first_name', 'LIKE', "%$busca%")->orWhere('last_name','LIKE',"%$busca%");
		}

		$usuarios = $usuarios->paginate(20);


		// dd( $usuarios->toArray() );

		return Response::view('usuarios.index',[
			"usuarios" => $usuarios
		]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$perfil = User::find($id);

		if ( $perfil ) {
			$perfil->load('professor');
			return Response::view('usuarios.perfil',[
				"perfil" => $perfil
			]);
		} else {
			App::abort(404);
		}
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$perfil = User::find($id);

		if ( $perfil ) {
			$profissoes = Config::get('infinito.profissoes', []);
			$profissoes = array_combine($profissoes, $profissoes);
			asort($profissoes);

			$profissoes[] = "Outros";

			return Response::view('usuarios.form',[
				"perfil"     => $perfil,
				"profissoes" => $profissoes,
			]);
		} else {
			App::abort(404);
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$perfil = User::find($id);

		if ( $perfil ) {

			$validator = Validator::make(Input::all(), User::$rules);

			if ($validator->fails()) {
				return Redirect::route("usuarios.edit", [$perfil->id])->withErrors($validator)->withInput( Input::all() );
			} else {

				$perfil->fill( Input::all() );
				$perfil->cadastro_completo = true;
				$perfil->save();

				return Redirect::route("usuarios.show", [$perfil->id])->with("message","Dados atualizados com sucesso");
			}
		} else {
			App::abort(404);
		}
	}


	/**
	 * Atualiza grupo de professor do usuario
	 *
	 * @param  int  $id
	 * @param  bool  $opcao
	 * @return Response
	 */
	public function professor($id, $opcao)
	{
		$perfil = User::find($id);

		if ( $perfil ) {

			if ( ! $this->user->isAdmin() ) {
				return Redirect::route("usuarios.show", [$perfil->id])->with("message","Você não tem permissão para fazer essa alteração");
			}

			$gProf = Sentry::findGroupByName('professor');

			if ( $opcao ) {
				$perfil->addGroup($gProf);
			} else {
				$perfil->removeGroup($gProf);
			}

			return Redirect::route("usuarios.show", [$perfil->id])->with("message","Perfil atualizados com sucesso");
		} else {
			App::abort(404);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	public function salvaFoto($tipo)
	{
		$retorno = Input::get('retorno');

		// dd($tipo);

		if ( ! Input::hasFile('imagem')) {
			return Redirect::to($retorno . "?image-error-$tipo=true");
		} else {
			$foto     = Input::file('imagem');
			$destino  = Config::get("infinito.UPLOAD_PATH");
			$fileName = $tipo . "_" . $this->user->id . "_" . uniqid() . "." . $foto->getClientOriginalExtension();

			$foto->move($destino, $fileName);

			if ( $tipo == "capa" ) {
				$this->user->imagem_capa = $fileName;
			} else {
				$this->user->imagem_perfil = $fileName;
			}
			$this->user->save();

			return Redirect::to($retorno);
		}
	}


}
