<?php

class MensagensController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$conversas = Conversa::where('autor_id','=',$this->user->id)->orWhere('destino_id','=',$this->user->id)->orderBy('updated_at','DESC')->orderBy('created_at','DESC')->paginate(20);

		return Response::view('mensagens.index',["conversas" => $conversas]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), [
			"from" => "required",
			"to" => "required",
			"mensagem" => "required",
			"assunto" => "required",
		]);

		$retorno = Input::get('retorno');

		if ($validator->fails()) {
			return Redirect::to( $retorno )->withErrors($validator)->withInput( Input::all() );
		} else {

			$conversa = Conversa::create([
				"assunto" => Input::get("assunto"),
			]);

			$autor = User::find( Input::get("from") );
			$destino = User::find( Input::get("to") );

			$conversa->autor()->associate($autor);
			$conversa->destino()->associate( $destino );
			$conversa->save();


			$mensagem = Mensagem::create([
				"texto" => Input::get("mensagem")
			]);

			$conversa->mensagens()->save($mensagem);
			$mensagem->conversa()->associate($conversa);
			$mensagem->autor()->associate($autor);

			$mensagem->save();

			$this->createHistorico(
				"Nova mensagem! <a href='" . route("mensagens.index") . "'>" . $conversa->assunto . "</a>",
				"envelope",
				$destino
			);

			// dd( $mensagem->toArray() );

			return Redirect::to($retorno)->with("message_nota","Mensagem enviada com sucesso");
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$conversa = Conversa::find($id);

		if ( $conversa ) {
			$conversa->load('mensagens');
			$conversa->mensagens->sortBy('created_at');
			
			return Response::view('mensagens.form',["conversa" => $conversa]);
		} else {
			App::abort(404);
		}

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$cid = Input::get("conversa");

		$conversa = Conversa::find($cid);

		if ( ! $conversa ) {
			return Redirect::route("mensagens.index")->with("message","A conversa não existe");
		}

		$validator = Validator::make(Input::all(), [
			"from" => "required",
			"to" => "required",
			"mensagem" => "required",
		]);

		if ($validator->fails()) {
			return Redirect::route("mensagens.show", [$conversa->id])->withErrors($validator)->withInput( Input::all() );
		} else {

			$mensagem = Mensagem::create([
				"texto" => Input::get("mensagem")
			]);

			$destino = User::find( Input::get("from") );

			$conversa->mensagens()->save($mensagem);
			$mensagem->conversa()->associate($conversa);
			$mensagem->autor()->associate( $destino );

			$mensagem->save();

			$this->createHistorico(
				"Nova mensagem! <a href='" . route("mensagens.index") . "'>" . $conversa->assunto . "</a>",
				"envelope",
				$destino
			);

			return Redirect::route("mensagens.show", [$conversa->id])->with("message","Mensagem enviada com sucesso");
		}

		return $this->index();
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
