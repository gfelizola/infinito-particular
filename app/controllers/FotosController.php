<?php

class FotosController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// dd( $this->user );
		// $this->user->load('albuns');
		// $albuns = $this->user->albuns->load('fotos');

		$albuns = Album::all();

		return Response::view('fotos.index',[
			'albuns' => $albuns
		]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function album($id = null)
	{
		if ( $id ) {
			$albuns = Album::findOrFail($id);
		} else {
			$albuns = Album::all();
			//$this->user->albuns->load('fotos');
		}

		if ( ! is_array($albuns) ) {
			$albuns = [$albuns];
		}

		// dd($albuns);
		return Response::view('fotos.fotos', [
			'albuns' => $albuns
		]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
