<?php

class VideosController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$videos = Video::orderBy('created_at','DESC')->paginate(20);
		$ids = [];
		$str = "";

		foreach ($videos as $v) {
			parse_str( parse_url( $v->url, PHP_URL_QUERY ), $tmp );
			if ( isset( $tmp['v'] ) && ! empty( $tmp['v'] ) ) {
				$ids[ $tmp['v'] ] = $v;
				$str .= ( ! empty($str) ? "," : "" ) . $tmp['v'];
			}
		}

		$dados = $this->youtubeData( $str );

		// dd($dados);
		
		foreach ($dados as $key => $value) {
			$o = $ids[ $value['id'] ];

			if ( is_object($o) ) {
				$ids[$value['id']] = [
					"id" => $o->id,
					"yid" => $value['id'],
					"title" => $value['snippet']['title'],
					"views" => $value['statistics']['viewCount'],
				];
			}
		}

		return Response::view('videos.index', [
			"videos" => $ids,
			"query"  => $videos
		]);
	}

	function youtubeData($id) {
		$url = "https://www.googleapis.com/youtube/v3/videos?id=" . $id . "&key=" . Config::get('infinito.YT_KEY') . "&fields=items(id,snippet(title),statistics)&part=snippet,statistics";

		$videoData = file_get_contents($url);

		if ($videoData) {
			$json = json_decode($videoData, true);

			return $json['items'];
		} else {
			return false;
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), array(
			'url' => 'required|unique:videos,url',
		));

		if ($validator->fails())
		{
			return Redirect::to("videos")->withErrors($validator)->withInput( Input::all() );
		}
		else
		{
			$url = Input::get("url");

			Video::create([
				"url" => $url,
				"nome" => uniqid()
			]);

			return $this->index();
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Video::destroy($id);

		return Response::make(json_encode(["sucesso" => true]));
	}


}
