<?php

class LoginController extends \BaseController {

    public function index()
    {
        if ( Sentry::check() )
        {
            return Redirect::to("/" );
        }

        return Response::view('login.login', array(
            "mensagem" => Session::pull("mensagem")
        ));
    }

    public function validate()
    {
        $validator = Validator::make(Input::all(), array(
            'email' => 'required|email',
            'senha' => 'required',
        ));

        if ($validator->fails())
        {
            return Redirect::to("login")->withErrors($validator)->withInput( Input::all() );
        }
        else
        {
            try
            {
                // Login credentials
                $credentials = array(
                    'email'    => Input::get('email'),
                    'password' => Input::get('senha'),
                );

                // Authenticate the user
                $user = Sentry::authenticate($credentials, Input::get('lembrar', false) );
            }
            catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
            {
                return Redirect::to("login")->withErrors(["mensagem" => "O campo login é obrigatório"])->withInput( Input::all() );
            }
            catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
            {
                return Redirect::to("login")->withErrors(["mensagem" => "O campo senha é obrigatório"])->withInput( Input::all() );
            }
            catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
            {
                return Redirect::to("login")->withErrors(["mensagem" => "Senha inválida, tente novamente"])->withInput( Input::all() );
            }
            catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
            {
                return Redirect::to("login")->withErrors(["mensagem" => "Usuário não encontrado"])->withInput( Input::all() );
            }
            catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
            {
                return Redirect::to("login")->withErrors(["mensagem" => "Este usuário ainda não está ativo"])->withInput( Input::all() );
            }
            catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
            {
                return Redirect::to("login")->withErrors(["mensagem" => "Usuário suspenso"])->withInput( Input::all() );
            }
            catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
            {
                return Redirect::to("login")->withErrors(["mensagem" => "Usuário bloqueado"])->withInput( Input::all() );
            }

            return Redirect::to( Session::pull("login_back", "/") );
        }
    }

    public function esqueci()
    {
        return Response::view('login.esqueci', array(
            "mensagem" => Session::pull("mensagem")
        ));
    }

    public function enviaSenha()
    {
        $validator = Validator::make(Input::all(), array(
            'email' => 'required|email'
        ));

        if ($validator->fails())
        {
            return Redirect::route("login.esqueci")->withErrors($validator)->withInput( Input::all() );
        }
        else
        {
            try
            {
                $email = Input::get('email');

                $user = Sentry::findUserByLogin( $email );
                $resetCode = $user->getResetPasswordCode();

                Mail::send( "emails.esqueci", [
                    "codigo" => $resetCode,
                    "id"     => $user->id,
                ], function($message) use($email){
                    $message->to($email);
                    $message->subject("Infinito - atualização de senha");
                });

                return Redirect::route("login")->withErrors(["mensagem" => "Seu lembrete foi enviado para seu e-mail"]);
            }
            catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
            {
                return Redirect::route("login.esqueci")->withErrors(["mensagem" => "Usuário não encontrado"])->withInput( Input::all() );
            }
        }
    }

    public function formSenha($id, $codigo)
    {
        return Response::view('login.reset-senha', array(
            "codigo"   => $codigo,
            "id"       => $id,
            "mensagem" => Session::pull("mensagem")
        ));
    }

    public function validaSenha()
    {
        $validator = Validator::make(Input::all(), array(
            'senha'          => 'required',
            'confirma_senha' => 'required'
        ));

        $id = Input::get("id");
        $codigo = Input::get("codigo");

        if ($validator->fails())
        {
            return Redirect::route("login.reset", [$id, $codigo])->withErrors($validator)->withInput( Input::all() );
        }
        else
        {
            try
            {
                $user = Sentry::findUserById( $id );

                if ($user->checkResetPasswordCode($codigo))
                {
                    // Attempt to reset the user password
                    if ($user->attemptResetPassword($codigo, Input::get('senha')))
                    {
                        return Redirect::route("login")->withErrors(["mensagem" => "Senha atualizada com sucesso"]);
                    }
                    else
                    {
                        return Redirect::route("login.reset", [$id, $codigo])->withErrors(["mensagem" => "Não foi possível trocar sua senha"])->withInput( Input::all() );
                    }
                }
                else
                {
                    return Redirect::route("login.reset", [$id, $codigo])->withErrors(["mensagem" => "Não foi possível trocar sua senha"])->withInput( Input::all() );
                }
            }
            catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
            {
                return Redirect::route("login.reset", [$id, $codigo])->withErrors(["mensagem" => "Usuário não encontrado"])->withInput( Input::all() );
            }
        }
    }

    public function logout()
    {
        Sentry::logout();
        return Redirect::to("login")->withErrors(["mensagem" => "Você foi deslogado."]);
    }

    public function cadastro()
    {
        if ( Sentry::check() )
        {
            return Redirect::to("/" );
        }

        $professores = Sentry::findAllUsersWithAccess(array('aluno'));

        array_unshift($professores, [""]);
        $lista_profs = [];
        $lista_profs[] = "Selecione seu professor";

        foreach ($professores as $prof) {
            if ( is_a( $prof, "User" ) ) {
                $lista_profs[$prof->id] = $prof->nome_completo;
            }
        }

        return Response::view('login.cadastro', array(
            "mensagem" => Session::pull("mensagem"),
            "professores" => $lista_profs,
        ));
    }

    public function cadastroSave()
    {
        $validator = Validator::make(Input::all(), array(
            'email'      => 'required|email|unique:users',
            'senha'      => 'required|confirmed',
            'first_name' => 'required',
            'last_name'  => 'required',
            'professor'  => 'required',
        ));

        if ($validator->fails())
        {
            return Redirect::to("cadastro")->withErrors($validator)->withInput( Input::all() );
        }
        else
        {
            try
            {
                Config::set('cartalyst/sentry::users.login_attribute', 'email');

                // Login credentials
                $credentials = array(
                    'email'        => Input::get('email'),
                    'password'     => Input::get('senha'),
                    'first_name'   => Input::get('first_name'),
                    'last_name'    => Input::get('last_name'),
                    'professor_id' => Input::get('professor'),
                );

                $user = Sentry::register($credentials, true);
                Sentry::login($user, true);
            }
            catch (Cartalyst\Sentry\Users\UserExistsException $e)
            {
                return Redirect::to("login")->withErrors(["mensagem" => "Este e-mail já está cadastrado"])->withInput( Input::all() );
            }

            return Redirect::to( Session::pull("login_back", "/") );
        }
    }

}
