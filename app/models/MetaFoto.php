<?php
class MetaFoto extends Eloquent {

	protected $table = "metas_fotos";
	protected $fillable = array('url');

	/*VALIDAÇÃO DOS CADASTROS*/
	public static $rules = array(
		'imagem' => 'required|image|max:5000',
	);

	/*TRANSFORMAÇÃO DAS DATAS*/
	public function getDates()
	{
		return array('created_at','updated_at');
	}

	public function meta()
	{
		return $this->belongsTo('Meta','meta_id');
	}

}
