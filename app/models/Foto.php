<?php

use Carbon\Carbon;

class Foto extends Eloquent {

	protected $fillable = array('nome', 'path', 'descricao');

	/*TRANSFORMAÇÃO DAS DATAS*/
	public function getDates()
	{
		return array('created_at','updated_at');
	}

	public function album()
	{
		return $this->belongsTo('Album');
	}
}
