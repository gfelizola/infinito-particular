<?php

use Carbon\Carbon;

class Meta extends Eloquent {

	const TIPO_ASANA = 1;
	const TIPO_RESPIRACAO = 2;
	const TIPO_MEDITACAO = 3;
	const TIPO_LIMPEZA = 4;

	protected $fillable = array('nome', 'meta', 'tipo', 'video', 'foto');

	/*VALIDAÇÃO DOS CADASTROS*/
	public static $rules = array(
		'nome' => 'required',
		'meta' => 'required',
		'tipo' => 'required',
	);

	/*TRANSFORMAÇÃO DAS DATAS*/
	public function getDates()
	{
		return array('created_at','updated_at');
	}

	public function aluno()
	{
		return $this->belongsTo('User','aluno_id');
	}

	public function notas()
	{
		return $this->hasMany('MetaNota','meta_id');
	}

	public function fotos()
	{
		return $this->hasMany('MetaFoto','meta_id');
	}

}
