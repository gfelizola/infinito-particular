<?php

use Carbon\Carbon;

class Chakra extends Eloquent {

	protected $fillable = array('nome', 'descricao', 'resumo', 'ordem');


	/*VALIDAÇÃO DOS CADASTROS*/
	public static $rules = array(
		'nome'      => 'required',
		'descricao' => 'required',
		'resumo'    => 'required',
	);

	/*TRANSFORMAÇÃO DAS DATAS*/
	public function getDates()
	{
		return array('created_at','updated_at');
	}

	public function professor()
	{
		return $this->belongsTo('User','professor_id');
	}

	public function tecnicas()
	{
		return $this->hasMany('Tecnica','chakra_id');
	}
}
