<?php

use Carbon\Carbon;

class Evento extends Eloquent {

	protected $fillable = array('nome', 'descricao', 'tipo', 'data_evento');


	/*VALIDAÇÃO DOS CADASTROS*/
	public static $rules = array(
		'nome'        => 'required',
		'data_evento' => 'required',
		'tipo'        => 'required',
	);

	/*TRANSFORMAÇÃO DAS DATAS*/
	public function getDates()
	{
		return array('created_at','updated_at','data_evento');
	}

	public function setDataEventoAttribute($value)
	{
		$this->attributes['data_evento'] = Carbon::createFromFormat('d/m/Y H:i', $value);
	}

	public function aluno()
	{
		return $this->belongsTo('User','aluno_id');
	}

}
