<?php

use Carbon\Carbon;

class Conversa extends Eloquent {

	protected $fillable = array('assunto');

	/*TRANSFORMAÇÃO DAS DATAS*/
	public function getDates()
	{
		return array('created_at','updated_at');
	}

	public function mensagens()
	{
		return $this->hasMany('Mensagem');
	}

	public function destino()
	{
		return $this->belongsTo('User');
	}

	public function autor()
	{
		return $this->belongsTo('User');
	}

}
