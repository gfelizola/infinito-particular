<?php

class Album extends Eloquent {

	protected $table = "albuns";

	protected $fillable = array('nome');

	/*TRANSFORMAÇÃO DAS DATAS*/
	public function getDates()
	{
		return array('created_at','updated_at');
	}

	public function user()
	{
		// return $this->belongsToMany('User', 'albuns_users');
		return $this->belongsTo('User');
	}

	public function fotos()
	{
		return $this->hasMany('Foto');
	}
}
