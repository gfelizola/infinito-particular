<?php

use Carbon\Carbon;

class Historico extends Eloquent {

	protected $fillable = array('mensagem','icone');

	/*VALIDAÇÃO DOS CADASTROS*/
	public static $rules = array(
		'nome'  => 'required',
	);

	public function user()
	{
		return $this->belongsTo('User','user_id');
	}
}
