<?php

use Carbon\Carbon;

class Tecnica extends Eloquent {

	protected $fillable = array('nome', 'tipo');

	/*VALIDAÇÃO DOS CADASTROS*/
	public static $rules = array(
		'nome'  => 'required',
	);

	public function chakra()
	{
		return $this->belongsTo('Chakra');
	}

	public function alunos()
	{
		return $this->belongsToMany('User','tecnicas_user','tecnica_id','user_id');
	}
}
