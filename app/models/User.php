<?php

use Carbon\Carbon;

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends \Cartalyst\Sentry\Users\Eloquent\User implements RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected static $loginAttribute = 'email';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array(
		'password',
		'reset_password_code',
		'activation_code',
		'persist_code',
		'remember_token',
	);

	protected $fillable = array(
		'email', 'professor_id','password','first_name', 'last_name', 'sexo', 'profissao', 'peso', 'altura', 'nacionalidade', 'telefone',
		'estado_civil', 'endereco', 'telefone', 'nacionalidade', 'objetivos', 'facebook',
		'data_nascimento', 'inicio_infinito', 'inicio_yoga', 'observacoes', 'dados_publicos',
		'imagem_perfil','imagem_capa','chakra'
	);


	/*VALIDAÇÃO DOS CADASTROS*/
	public static $rules = array(
		'first_name'      => 'required',
		'last_name'       => 'required',
		'sexo'            => 'required',
		'estado_civil'    => 'required',
		// 'peso'            => 'required',
		// 'altura'          => 'required',
		'nacionalidade'   => 'required',
		'telefone'        => 'required',
		'endereco'        => 'required',
		'objetivos'       => 'required',
		'data_nascimento' => 'required',
	);

	/*TRANSFORMAÇÃO DAS DATAS*/
	public function getDates()
	{
		return array('created_at','updated_at','data_nascimento','inicio_infinito','inicio_yoga');
	}

	/*** PROPRIEDADES ESPECIAIS ****/
	public function setInicioYogaAttribute($value)
	{
		$this->attributes['inicio_yoga'] = Carbon::createFromFormat('d/m/Y', $value);
	}

	public function setInicioInfinitoAttribute($value)
	{
		$this->attributes['inicio_infinito'] = Carbon::createFromFormat('d/m/Y', $value);
	}

	public function setDataNascimentoAttribute($value)
	{
		$this->attributes['data_nascimento'] = Carbon::createFromFormat('d/m/Y', $value);
	}

	public function getNomeCompletoAttribute()
	{
		return $this->first_name . " " . $this->last_name;
	}

	public function getSexoNomeAttribute()
	{
		$opcoes = Config::get('infinito.sexo');
		if ( isset( $opcoes[ $this->sexo ] ) ) return $opcoes[ $this->sexo ];
		return "";
	}

	public function getEstadoCivilNomeAttribute()
	{
		$opcoes = Config::get('infinito.estado_civil');
		if ( isset( $opcoes[ $this->estado_civil ] ) ) return $opcoes[ $this->estado_civil ];
		return "";
	}

	public function getNacionalidadeNomeAttribute()
	{
		$opcoes = Config::get('infinito.nacionalidade');
		if ( isset( $opcoes[ $this->nacionalidade ] ) ) return $opcoes[ $this->nacionalidade ];
		return "";
	}

	/*** RELACIONAMENTOS ****/
	public function albuns()
	{
		return $this->hasMany('Album');
	}

	public function professor()
	{
		return $this->hasOne('User', 'id', 'professor_id');
	}

	public function alunos()
	{
		return $this->hasMany('User', 'professor_id');
	}

	public function chakras()
	{
		return $this->hasMany('Chakra','professor_id');
	}

	public function eventos()
	{
		return $this->hasMany('Evento','aluno_id');
	}

	public function metas()
	{
		return $this->hasMany('Meta','aluno_id');
	}

	public function tecnicas()
	{
		return $this->belongsToMany('Tecnica','tecnicas_user','user_id','tecnica_id');
	}

	public function historicos()
	{
		return $this->hasMany('Historico','user_id');
	}

	/*** AUXILIARES ****/
	public function isProfessor()
	{
		$permissoes = $this->getMergedPermissions();
		if ( isset( $permissoes['aluno'] ) && $permissoes['aluno'] ) {
			return true;
		}

		return false;
	}

	/*** AUXILIARES ****/
	public function isAdmin()
	{
		$permissoes = $this->getMergedPermissions();
		if ( isset( $permissoes['admin'] ) && $permissoes['admin'] ) {
			return true;
		}

		return false;
	}
}
