<?php

use Carbon\Carbon;

class Mensagem extends Eloquent {

	protected $table = "mensagens";

	protected $fillable = array('texto', 'enviado_at');

	protected $touches = array('conversa');


	/*VALIDAÇÃO DOS CADASTROS*/
	public static $rules = array(
		'assunto' => 'required',
		'texto'   => 'required',
	);

	/*TRANSFORMAÇÃO DAS DATAS*/
	public function getDates()
	{
		return array('created_at','updated_at','enviado_at');
	}

	public function setEnviadoAtAttribute($value)
	{
		$this->attributes['enviado_at'] = Carbon::createFromFormat('d/m/Y', $value);
	}

	public function conversa()
	{
		return $this->belongsTo('Conversa');
	}

	public function autor()
	{
		return $this->belongsTo('User','autor_id');
	}

}
