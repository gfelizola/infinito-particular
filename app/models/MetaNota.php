<?php
class MetaNota extends Eloquent {

	protected $table = "metas_notas";
	protected $fillable = array('valor', 'descricao');

	/*VALIDAÇÃO DOS CADASTROS*/
	public static $rules = array(
		'valor'     => 'required|integer|max:100|min:0',
		'descricao' => 'required',
	);

	/*TRANSFORMAÇÃO DAS DATAS*/
	public function getDates()
	{
		return array('created_at','updated_at');
	}

	public function meta()
	{
		return $this->belongsTo('Meta','meta_id');
	}

}
