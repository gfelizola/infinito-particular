<?php

use Carbon\Carbon;

class Video extends Eloquent {

	protected $fillable = array('nome', 'url');

	/*VALIDAÇÃO DOS CADASTROS*/
	public static $rules = array(
		'url'  => 'required',
	);

	/*TRANSFORMAÇÃO DAS DATAS*/
	public function getDates()
	{
		return array('created_at','updated_at');
	}

	public function users()
	{
		return $this->belongsToMany('User', 'videos_users');
	}
}
