module.exports = function(grunt) {
    "use strict";

    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),

        cacheBust: (new Date()).getTime(),

        dirs: {
            base     : "./public_html",
            js       : {
                dest : "./public_html/js",
                src  : "./public_html/js/src",
                temp : "./public_html/js/src/_temp"
            },
            css      : {
                dest : "./public_html/css",
                src  : "./public_html/css/less"
            },
            img      : "./public_html/img",
            views    : "./app/views",
            ctrl     : "./app/controllers",
            classes  : "./app/classes",
        },

        concat: {
            options       : {
                separator : ""
            },
            frameworks    : {
                src       : [
                    //"<%= dirs.js.src %>/jquery/jquery-2.1.3.min.js"
                    //"<%= dirs.js.src %>/bootstrap/*.js"
                ],
                dest      : "<%= dirs.js.temp %>/frameworks.js"
            },
            plugins       : {
                src       : [
                    "<%= dirs.js.src %>/plugins/jquery.maskedinput.min.js",
                    "./bower_components/underscore/underscore.js",
                    "./bower_components/bootstrap-calendar/js/calendar.js",
                    "./bower_components/bootstrap-calendar/js/language/pt-BR.js",
                    "./bower_components/moment/moment.js",
                    "./bower_components/moment/locale/pt-br.js",
                    "./bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js",
                    "./bower_components/jquery-ui/jquery-ui.js",
                    "./bower_components/jquery-ui/ui/i18n/datepicker-pt-BR.js",
                    "./bower_components/flexslider/jquery.flexslider.js",
                ],
                dest      : "<%= dirs.js.dest %>/plugins.js"
            },
            app           : {
                src       : [
                    "<%= dirs.js.src %>/app*.js"
                ],
                dest      : "<%= dirs.js.temp %>/app.js"
            },
            dev           : {
                src       : [
                    "<%= dirs.js.temp %>/frameworks.js",
                    "<%= dirs.js.temp %>/plugins.js",
                    "<%= dirs.js.temp %>/app.js"
                ],
                dest      : "<%= dirs.js.src %>/script.js"
            }
        },

        // Minificar JS
        uglify: {
            options                 : {
                mangle              : {
                    //essa opção "mangle" é quem modifica os nomes de variáveis para palavras pequenas ou letras.
                    //no Array abaixo, podemos ignorar algumas variaveis.
                    except          : ["jQuery"]
                }
            },
            prod                    : {
                options             : {
                    banner          : "/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today('yyyy-mm-dd') %> */",
                    properties      : true,
                    preserveComments: false,
                    compress        : {
                        global_defs : {
                            "DEBUG" : false
                        },
                        dead_code   : true
                    }
                },
                files               : {
                    "<%= dirs.js.dest %>/script.min.js": ["<%= dirs.js.src %>/script.js"],
                    "<%= dirs.js.dest %>/plugins.min.js": ["<%= dirs.js.temp %>/plugins.js"],
                }
            }
        },

        less: {
            dev              : {
                options      : {
                    compress : false,
                    dumpLineNumbers: true
                },
                files        : {
                    "<%= dirs.css.dest %>/style.css": "<%= dirs.css.src %>/style.less"
                }
            }
        },

        //Sprite Generator
        sprite:{
            options       : {
                cssFormat : ".less"
            },
            infinito:{
                src             : "<%= dirs.img %>/sprites/*.png",
                retinaSrcFilter : "<%= dirs.img %>/sprites/*-2x.png",
                dest            : "<%= dirs.img %>/infinito-sprite.png",
                retinaDest      : "<%= dirs.img %>/infinito-sprite@2x.png",
                destCss         : "<%= dirs.css.src %>/sprites.less",
                imgPath         : "../img/infinito-sprite.png?v=<%=cacheBust%>",
                retinaImgPath   : "../img/infinito-sprite@2x.png?v=<%=cacheBust%>",
                padding         : 5
            }
        },

        // Watch
        watch: {
            options: {
                livereload: true
            },
            less: {
                files: "<%= dirs.css.src %>/**/*.less",
                tasks: ["less:dev"],
                options: {
                    livereload: false
                }
            },
            css: {
                files: ["<%= dirs.css.dest %>/*.css"]
            },
            js: {
                files: [
                    "Gruntfile.js",
                    "<%= dirs.js.src %>/**/*.js",
                    "<%= dirs.js.dest %>/main.js",
                    "!<%= dirs.js.src %>/script.js",
                    "!<%= dirs.js.dest %>/script.min.js",
                    "!<%= dirs.js.temp %>/*.js"
                ],
                tasks: ["js"]
            },
            others: {
                files: [
                    "<%= dirs.base %>/*.{html,txt,php}",
                    "<%= dirs.views %>/**/*.php",
                    "<%= dirs.ctrl %>/**/*.php"

                ]
            }
        },

        //browserSync - Sincroniza desktop e mobile
        browserSync: {
            home: {
                options: {
                    watchTask: true,
                    proxy: "dev.infinito.com.br"
                },
                bsFiles: {
                    injectChanges: false,
                    src : [
                        "<%= dirs.css.dest %>/*.css",
                        "<%= dirs.js.src %>/script.js",
                        "<%= dirs.base %>/*.{html,txt,php}",
                        "<%= dirs.views %>/**/*.php",
                        "<%= dirs.ctrl %>/**/*.php"
                    ]
                }
            }
        }
    });

    grunt.registerTask("bs-inject", function () {
        browserSync.reload(["styles.css", "other.css"]);
    });

    grunt.registerTask("w", ["build","browserSync","watch"]);
    grunt.registerTask("css", ["less"]);
    grunt.registerTask("js", ["concat","uglify"]);
    grunt.registerTask("front", ["css","js"]);
    grunt.registerTask("build", ["sprite","less","js"]);
    grunt.registerTask("default", ["build"]);
};
