$(function() {
	

	// =================================
	// VIDEOS
	// =================================

	$(".btn-excluir-video").click(function(event) {
		event.preventDefault();

		if ( confirm("Vocé realmente deseja apagar este vídeo?") ) {
			$.ajax({
				url: $(this).attr('href'),
				type: 'DELETE',
				success: function(result) {
					// console.log(result);
					window.location.href = window.location.href;
				}
			});
		};
	});

	// =================================
	// EVENTOS
	// =================================

	var calendar = $("#calendario").calendar(
	{
		tmpl_path     : "/vendor/bootstrap-calendar/tmpls/",
		language      : 'pt-BR',
		time_split    : '60',
		modal         : "#events-modal",
		modal_type    : "template",
		events_source : window.eventos || [],
		modal_title   : function(e){
			return e.title;
		},
		views   : {
			day : { enable: false }
		},
		onAfterViewLoad: function(view) {
			$('.calendar-header h3').text(this.getTitle());
		},
	});


	var calOpts = $.datepicker.regional['pt-BR'];
	calOpts.inline = true;
	calOpts.beforeShowDay = function(data){
		var dia = [data.getDate(), data.getMonth() + 1,data.getFullYear()].join('/'),
			retorno = [true];

		if ( $.isArray( userEvents ) ) {
			$.each(userEvents, function(index, evento) {
				if ( evento.data == dia ) {
					retorno.push('tem-evento');
				};
			});
		};

		return retorno;
	};
	calOpts.onSelect = function(dataText) {
		window.location.href = '/eventos/calendar/' + window.user.id;
	}

	$('.eventos .btn-group a[data-calendar-nav]').click(function(event) {
		event.preventDefault();
		calendar.navigate($(this).data('calendar-nav'));
	});

	$('#miniCalendario').datepicker(calOpts);

	// $( "#data_evento" ).datepicker();

	var inicial = $( "#data_evento" ).val();
	// if ( inicial.length ) {
	// 	var iData = inicial.split(" ")[0];
	// 	var iHora = inicial.split(" ")[1];
		
	// 	var dSpl = iData.split("-");

	// 	inicial = dSpl[2] + "/" + dSpl[1] + "/" + dSpl[0] + " " + iHora;
	// };
	console.log( inicial );
	var m = moment(inicial, "YYYY-MM-DD HH:mm:ss");
	$( "#data_evento" ).val('');
	$( "#data_evento" ).datetimepicker({
		sideBySide: true,
		defaultDate: m
	});

	$(document).on("click",".btn-editar-evento",function(event) {
		event.preventDefault();
		var eventoID = $(this).parents(".modal-content").find("#event-meta").data('evento-id');

		var url = $(this).attr('href');
		var urlArr = url.split("/");
		urlArr.pop();
		url = urlArr.join("/");
		url += "/" + eventoID;

		// console.log( url );

		window.location.href = url;
	});

	$(document).on("click",".btn-excluir-evento",function(event) {
		event.preventDefault();
		var eventoID = $(this).parents(".modal-content").find("#event-meta").data('evento-id');

		if ( eventoID && confirm("Vocé realmente deseja apagar este evento?") ) {
			$.ajax({
				url: "/eventos/" + eventoID,
				type: 'DELETE',
				success: function(result) {
					window.location.href = window.location.href;
				}
			});
		}
	});

	// ===================
	// METAS
	// ===================

	if ( $('.meta-item .fotos .slider li').length > 5 ) {
		$('.meta-item .fotos').flexslider({
			selector: '.slider li',
			animation: "slide",
			animationLoop: false,
			itemWidth: 140,
			itemMargin: 10,
			slideshow: false,
			controlNav: false,
			move: 1
		});
	}


	$(".link-meta").fancybox({
		padding: 10
	});

	$('.btn-add-nota').click(function(event) {
		event.preventDefault();
		$('.form-nota').slideToggle(500, function(){
			window.top.$.fancybox.update();
		});
	});

	$('.link-foto').click(function(event) {
		event.preventDefault();

		var slider = $(this).parents('.slider');
		var fotos = [];
		slider.find('li').each(function(index, el) {
			fotos.push( $(el).find('a').attr('href') );
		});
		window.top.$.fancybox( fotos, { index : $(this).parent('li').index() } );
	});

	

	// ===================
	// PERFORMANCE / CONHECIMENTO
	// ===================

	if ( $('.lista-tecnicas form').length ) {
		$('.lista-tecnicas form .check-tecnica').click(function(event) {
			// console.log( "Form atualizado" );
			// $('.lista-tecnicas form').submit();
		});
	};
	

	// ===================
	// GERAIS
	// ===================

	if ( window.location.href.toLowerCase().indexOf('image-error') > 0 ) {
		var url = window.location.href.toLowerCase();
		var ar = url.match(/image\-error-(\w+)\=/);
		$('.btn-imagem-' + ar[1]).click();
	};

	$(".date-field").mask("99/99/9999",{placeholder:"dd/mm/yyyy"});
	$(".phone-field").mask("(99) 9999-9999?9");

	$('.btn-open-menu').click(function(event) {
		event.preventDefault();
		$('#main-menu ul li').toggle();
	});

	$('.btn-toggle').click(function(event) {
		event.preventDefault();
		$( $(this).attr('href') ).slideToggle(500);
	});

	$(".fancybox").fancybox({
		padding: 0
	});

	$('.btn-cancelar').click(function(event) {
		event.preventDefault();
		$.fancybox.close();
	});
});

$(window).load(function() {
	$('.metas-container.flexslider').each(function(index, el) {
		var qtd = $(el).find('li').length;
		var min = $(window).width() > 700 ? 4 : 2;
		if ( qtd >= min ) {
			$(el).flexslider({
				selector: '.slider li',
				animation: "slide",
				animationLoop: false,
				itemWidth: 220,
				itemMargin: 0,
				slideshow: false,
				controlNav: false,
				move: 1
			});
		};
	});  

	
});